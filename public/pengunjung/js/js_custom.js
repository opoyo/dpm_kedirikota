tinymce.init({
  selector: 'textarea',
  language: 'id',
  statusbar: true,
  menubar: false,
  plugins : 'image imagetools',
  branding: false,
  height: 300,
  theme_advanced_resizing : true,
  file_picker_types: 'image',
  file_picker_callback: function(cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.onchange = function() {
      var file = this.files[0];
      var reader = new FileReader();
      reader.onload = function () {
        var id = 'blobid' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };
    
    input.click();
  },
  theme: "modern",
  toolbar: 'formatselect | bold italic alignleft aligncenter alignright bullist numlist removeformat | image',
  language_url: '/portal_mojokertokota/assets/js/tinymce/langs/id.js',
});
function handleFiles(files) {
  for (var i = 0; i < files.length; i++) {
    var file = files[i];
    if (!file.type.startsWith('image/')){ continue }
    var imgblob="";
	var randID=Math.floor(Math.random() * 10000);
    var img = document.createElement("img");
    img.setAttribute("onclick", "img_"+randID+".remove();this.remove()");
    img.classList.add("obj-upload");
    img.file = file;
    preview.appendChild(img);
    var reader = new FileReader();
    reader.onload = (function(aImg) { 
		return function(e) { 
			aImg.src = e.target.result;
			var imgblob=aImg.src; 
			var input = document.createElement("input");
			input.setAttribute("type", "hidden");
			input.setAttribute("name", "img["+randID+"]");
			input.setAttribute("id", "img_"+randID);
			input.setAttribute("value", imgblob);
			addHiden.appendChild(input);

		};
	})(img);
    reader.readAsDataURL(file);
  }
}