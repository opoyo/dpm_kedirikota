<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeritaModel extends Model
{
  protected $table = 'berita';
  protected $primaryKey = 'berita_id';
  protected $fillable = ['berita_nama','berita_isi','berita_tanggal','berita_gambar','users_id'];

  public function user(){
    return $this->belongsTo('App\UserModel');
  }
}
