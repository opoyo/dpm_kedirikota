<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelayangPandang;
use Image;
use Carbon\Carbon;
use File;
use Alert;

class AdminSelayangPandangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $selayang_pandang = SelayangPandang::find(1);
        return view('admin.selayang_pandang.index', compact('selayang_pandang'));
        // return $selayang_pandang;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $detail = $request->input('selayang_pandang_isi');
      $selayang_pandang_isi=null;
      if ($detail == !null) {
        libxml_use_internal_errors(true);
        $dom = new \DomDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $selayang_pandang_isi = $dom->saveHTML();
      }
      $selayang_pandang = SelayangPandang::find($id);
      if($selayang_pandang_isi == !null){
        $selayang_pandang->selayang_pandang_isi = $selayang_pandang_isi;
      }
      $selayang_pandang->update();

      Alert::success('Data berhasil diubah', 'Success');
      return redirect('admin_selayang_pandang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
