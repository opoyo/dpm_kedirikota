<?php

namespace App\Http\Controllers;
use App\PengaduanModel as Pengaduan;

use Illuminate\Http\Request;

class AdminPengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pengaduan = Pengaduan::orderBy('created_at','ASC')->get();
        return view('admin.pengaduan.index',compact('pengaduan'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // dd($id);
      $pengaduan = Pengaduan::find($id);
        return view('admin.pengaduan.show',compact('pengaduan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengaduan = Pengaduan::find($id);
        $pengaduan->delete();
        return back();
    }
    public function balas_pengaduan(Request $request)
    {
      $pengaduan = Pengaduan::findOrFail($request->id_pengaduan);
      $pengaduan->pengaduan_balas = $request->pengaduan_balas;
      $pengaduan->pengaduan_status_balas = 1;
      $pengaduan->update();
      return back();
    }
    public function update_checkbox(Request $request)
    {
      // $jmlRekom = Program::where('program_rekomendasi',1)->count();
   $pengaduan = Pengaduan::findOrFail($request->id);
     if($pengaduan->pengaduan_aktif == 1){
         $pengaduan->pengaduan_aktif = 2;
     } else {
         $pengaduan->pengaduan_aktif = 1;
     }

   return response()->json([
       'data' => [
         'success' => $pengaduan->update(),
       ]
     ]);
    }
}
