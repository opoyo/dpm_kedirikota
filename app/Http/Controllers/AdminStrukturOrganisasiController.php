<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StrukturOrganisasi;
use Image;
use File;

class AdminStrukturOrganisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $struktur_organisasi = StrukturOrganisasi::select('struktur_organisasi_id','struktur_organisasi_gambar')->orderBy('struktur_organisasi_id','ASC')->get();
      $struktur_organisasi_count = StrukturOrganisasi::count();
      return view('admin.struktur_organisasi.index',compact('struktur_organisasi','struktur_organisasi_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $messages = array(
        'struktur_organisasi_gambar.required'  => 'Gambar tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
          'file'    => 'File tidak boleh lebih dari :max kilobytes.',
          ],
        );
        $this->validate($request, [
          'struktur_organisasi_gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ],$messages);

        $struktur_organisasi = new StrukturOrganisasi;

        if ($request->hasFile('struktur_organisasi_gambar')) {
          $filerequest = $request->file('struktur_organisasi_gambar');
          $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
          Image::make($filerequest)->resize(3152, 2017, function ($constraint) {
            $constraint->aspectRatio();
          })->save(public_path('/pengunjung/images/struktur_organisasi/' . $filename));
          Image::make($filerequest)->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
          })->save(public_path('/pengunjung/images/struktur_organisasi/tumb/' . $filename));

          $struktur_organisasi->struktur_organisasi_gambar = $filename;
          $struktur_organisasi->save();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $struktur_organisasi = StrukturOrganisasi::find($id);
      $path = public_path() . '/pengunjung/images/struktur_organisasi/';
      File::delete($path. $struktur_organisasi->struktur_organisasi_gambar);

      $path = public_path() . '/pengunjung/images/struktur_organisasi/tumb/';
      File::delete($path. $struktur_organisasi->struktur_organisasi_gambar);
      $struktur_organisasi->delete();
      return back();
    }
}
