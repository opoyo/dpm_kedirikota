<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tupoksi;
use Image;
use Carbon\Carbon;
use File;
use Alert;

class AdminTupoksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tupoksi = Tupoksi::find(1);
      return view('admin.tupoksi.index', compact('tupoksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $detail = $request->input('tupoksi_isi');
      $tupoksi_isi=null;
      if ($detail == !null) {
        libxml_use_internal_errors(true);
        $dom = new \DomDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $tupoksi_isi = $dom->saveHTML();
      }
      $tupoksi = Tupoksi::find($id);
      if($tupoksi_isi == !null){
        $tupoksi->tupoksi_isi = $tupoksi_isi;
      }
      $tupoksi->update();

      Alert::success('Data berhasil diubah', 'Success');
      return redirect('admin_tupoksi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
