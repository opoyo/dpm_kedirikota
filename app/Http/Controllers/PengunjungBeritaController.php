<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BeritaModel as Berita;

class PengunjungBeritaController extends Controller
{
  public function index()
  {

    $batas=6;
    $berita = Berita::orderBy('berita_id', 'desc')->paginate($batas);

    $no=$batas*($berita->currentPage()-1);

    return view("pengunjung.publikasi.berita", compact('berita', 'no'));
  }

  public function detail($id)
  {
      // return($id);

      $berita_detail = Berita::join('users', 'berita.users_id','=','users.id')->where('berita_id','=',$id)->get();
      $berita = Berita::orderBy('berita_id', 'desc')->limit(6)->get();

      // dd($berita);

      return view("pengunjung.publikasi.berita_detail", compact('berita_detail','berita'));

  }

  public function cari(Request $request)
  {
    $cari = $request['cari'];

    $batas=6;
    $berita = Berita::where('berita_nama', 'like', '%'.$cari.'%')
    ->orWhere('berita_isi', 'like', '%'.$cari.'%')
    ->orderBy('berita_id', 'desc')->paginate($batas);

    $no=$batas*($berita->currentPage()-1);

    return view("pengunjung.publikasi.berita_search", compact('berita', 'no','cari'));

    // dd($berita);
  }

}
