<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GaleriModel as Galeri;

class PengunjungGaleriController extends Controller
{
    public function index()
    {
      $galeri = Galeri::where('galeri_id_parent','=','0')->get();
      return view('pengunjung.galeri.galeri', compact('galeri'));
      // dd($galeri);
    }

    public function detail($id)
    {
      $galeri = Galeri::find($id);
      $galeri_konten = Galeri::where('galeri_id_parent','=',$id)->get();
      return view('pengunjung.galeri.detail', compact('galeri','galeri_konten'));
      // dd($galeri_konten);
    }
}
