<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BiodataPejabatModel as BiodataPejabat;
use App\StrukturOrganisasi;

class PengunjungStrukturOrganisasiController extends Controller
{
  public function index(){

    $biodata = BiodataPejabat::All();
    $struktur_organisasi = StrukturOrganisasi::All();

    return view('pengunjung.tentang.struktur_organisasi', compact('biodata','struktur_organisasi'));
  }
}
