<?php

namespace App\Http\Controllers;

use App\PopupModel as Popup;
use App\PopupCheckModel;
use Illuminate\Http\Request;
use Image;
use File;

class AdminPopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $popup = Popup::select('popup_id','popup_gambar')->orderBy('created_at','DESC')->get();
      $popupCount = Popup::count();
      $popup_check = PopupCheckModel::All();
      return view('admin.popup.index',compact('popup','popupCount','popup_check'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $messages = array(
        'popup_gambar.required'  => 'Gambar tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'popup_gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
      ],$messages);
      $popup = new Popup;

      if ($request->hasFile('popup_gambar')) {
        $filerequest = $request->file('popup_gambar');
        $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
        Image::make($filerequest)->resize(560, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/popup/' . $filename));
        Image::make($filerequest)->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/popup/tumb/' . $filename));

        $popup->popup_nama = $request['popup_nama'];
        $popup->popup_gambar = $filename;
        $popup->save();
      }
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PopupModel  $popupModel
     * @return \Illuminate\Http\Response
     */
    public function show(PopupModel $popupModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PopupModel  $popupModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PopupModel $popupModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PopupModel  $popupModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PopupModel $popupModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PopupModel  $popupModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $popup = Popup::find($id);
      $path = public_path() . '/pengunjung/images/popup/';
       File::delete($path. $popup->popup_gambar);

       $path = public_path() . '/pengunjung/images/popup/tumb/';
        File::delete($path. $popup->popup_gambar);
        $popup->delete();
        return back();
    }

    public function update_checkbox(Request $request)
    {
      $popup_check = PopupCheckModel::findOrFail($request->id);
      if($popup_check->popup_check_value == 1){
        $popup_check->popup_check_value = 0;
      } else {
        $popup_check->popup_check_value = 1;
      }

      return response()->json([
        'data' => [
          'success' => $popup_check->update(),
          'id' => $request->id,
        ]
      ]);
    }
}
