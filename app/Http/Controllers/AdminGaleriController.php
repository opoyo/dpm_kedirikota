<?php

namespace App\Http\Controllers;

use App\GaleriModel as Galeri;
use Illuminate\Http\Request;
use Image;
use File;
use Validator;

class AdminGaleriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $galeri = Galeri::select('galeri_id','galeri_nama','galeri_gambar')
                ->where('galeri_id_parent','=','0')
                ->orderBy('created_at','DESC')->get();
      $combo = Galeri::select('galeri_id','galeri_nama')
                ->where('galeri_id_parent','=','0')->get();

                // dd($combo);

      return view('admin.galeri.index',compact('galeri','combo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $messages = array(
        'galeri_gambar.required'  => 'Gambar tidak boleh kosong.',
        'galeri_nama.required'    => 'Judul tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'galeri_gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'galeri_nama' => 'required',
      ],$messages);
      
      $galeri = new Galeri;

      if ($request->hasFile('galeri_gambar')) {
        $filerequest = $request->file('galeri_gambar');
        $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
        Image::make($filerequest)->resize(560, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/galeri/' . $filename));
        Image::make($filerequest)->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/galeri/tumb/' . $filename));

        $galeri->galeri_nama = $request['galeri_nama'];
        $galeri->galeri_id_parent = $request['galeri_id_parent'];
        $galeri->galeri_gambar = $filename;
        $galeri->save();
      }
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $galeri = Galeri::find($id);

      $path = public_path() . '/pengunjung/images/galeri/';
      File::delete($path. $galeri->galeri_gambar);

      $path = public_path() . '/pengunjung/images/galeri/tumb/';
      File::delete($path. $galeri->galeri_gambar);

      $galeri_select = Galeri::where('galeri_id_parent','=',$id)->get();

      foreach ($galeri_select as $data ) {
        $path = public_path() . '/pengunjung/images/galeri/';
        File::delete($path. $data->galeri_gambar);

        $path = public_path() . '/pengunjung/images/galeri/tumb/';
        File::delete($path. $data->galeri_gambar);
      }


      $deletedRows = Galeri::where('galeri_id_parent', $id)->delete();
      $galeri->delete();
      return back();
    }
}
