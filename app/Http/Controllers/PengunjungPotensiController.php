<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PotensiModel as Potensi;

class PengunjungPotensiController extends Controller
{
    public function index()
    {

      $batas=6;
      $potensi = Potensi::orderBy('potensi_id', 'desc')->paginate($batas);

      $no=$batas*($potensi->currentPage()-1);

      return view("pengunjung.potensi.potensi", compact('potensi', 'no'));

    }

    public function detail($id)
    {
        // return($id);

        $potensi_detail = Potensi::where('potensi_id','=',$id)->get();
        $potensi = Potensi::orderBy('potensi_id', 'desc')->limit(6)->get();

        // dd($berita);

        return view("pengunjung.potensi.potensi_detail", compact('potensi_detail','potensi'));

    }

}
