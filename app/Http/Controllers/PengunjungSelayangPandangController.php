<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SelayangPandang;

class PengunjungSelayangPandangController extends Controller
{
  public function index()
  {
    $selayang_pandang = SelayangPandang::find(1);
    return view('pengunjung.tentang.selayang_pandang', compact('selayang_pandang'));
  }
}
