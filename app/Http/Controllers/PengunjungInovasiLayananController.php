<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InovasiLayananModel as InovasiLayanan;

class PengunjungInovasiLayananController extends Controller
{
  public function index()
  {

    $batas=6;
    $inovasi_layanan = InovasiLayanan::orderBy('inovasi_layanan_id', 'desc')->paginate($batas);

    $no=$batas*($inovasi_layanan->currentPage()-1);

    return view("pengunjung.inovasi_layanan.inovasi_layanan", compact('inovasi_layanan', 'no'));

  }

  public function detail($id)
  {
      // return($id);

      $inovasi_layanan_detail = InovasiLayanan::where('inovasi_layanan_id','=',$id)->get();
      $inovasi_layanan = InovasiLayanan::orderBy('inovasi_layanan_id', 'desc')->limit(6)->get();

      // dd($berita);

      return view("pengunjung.inovasi_layanan.inovasi_layanan_detail", compact('inovasi_layanan_detail','inovasi_layanan'));

  }
}
