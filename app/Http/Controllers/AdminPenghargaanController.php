<?php

namespace App\Http\Controllers;

use App\PenghargaanModel as Penghargaan;
use Illuminate\Http\Request;
use File;
use Image;
class AdminPenghargaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penghargaan = Penghargaan::select('penghargaan_id','penghargaan_judul','penghargaan_gambar')->get();
        // dd($penghargaan);
        return view('admin.penghargaan.index',compact('penghargaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = array(
        'penghargaan_gambar.required'  => 'Gambar tidak boleh kosong.',
        'penghargaan_judul.required'    => 'Judul tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'penghargaan_gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
            'penghargaan_judul' => 'required',
      ],$messages);

      // $user_id = auth()->user()->id;
      $penghargaan = new Penghargaan;

      if ($request->hasFile('penghargaan_gambar')) {
        $filerequest = $request->file('penghargaan_gambar');
        $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
        Image::make($filerequest)->resize(403, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/penghargaan/' . $filename));
        Image::make($filerequest)->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/penghargaan/tumb/' . $filename));

        $penghargaan->penghargaan_judul = $request['penghargaan_judul'];
        $penghargaan->penghargaan_gambar = $filename;
        $penghargaan->save();
      }
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PenghargaanModel  $penghargaanModel
     * @return \Illuminate\Http\Response
     */
    public function show(PenghargaanModel $penghargaanModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PenghargaanModel  $penghargaanModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PenghargaanModel $penghargaanModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PenghargaanModel  $penghargaanModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PenghargaanModel $penghargaanModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PenghargaanModel  $penghargaanModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penghargaan = Penghargaan::find($id);
        $path = public_path() . '/pengunjung/images/penghargaan/';
         File::delete($path. $penghargaan->penghargaan_gambar);

         $path = public_path() . '/pengunjung/images/penghargaan/tumb/';
          File::delete($path. $penghargaan->penghargaan_gambar);
        $penghargaan->delete();
        return back();
    }
}
