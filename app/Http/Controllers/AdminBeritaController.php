<?php

namespace App\Http\Controllers;

use App\BeritaModel as Berita;
use Illuminate\Http\Request;
use Image;
use App\UserModel as User;
use Carbon\Carbon;
use File;

class AdminBeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $berita = Berita::select('berita_nama','berita_id')->orderBy('created_at','DESC')->get();
        return view('admin.berita.index',compact('berita'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $this->validate($request, [
      //
      //     'detail' => 'required',
      //
      // ]);

      $detail=$request->input('berita_isi');
      $berita_gambar='';
      $berita_isi='';
      if ($detail == !null) {

      $dom = new \DomDocument();
      $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $images = $dom->getElementsByTagName('img');
      foreach($images as $k => $img){
      $img->parentNode->removeChild($img);
      $data = $img->getAttribute('src');

      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);

      $data = base64_decode($data);
      $created_name = time().$k.'.jpeg';
      $image_name= "/pengunjung/images/berita/" .$created_name ;
      Image::make($data)->resize(600, null, function ($constraint) {
                     $constraint->aspectRatio();
                 })->save(public_path($image_name));
      $img->removeAttribute('src');
      $img->setAttribute('src', $image_name);
      $berita_gambar = $created_name;
    }
      $berita_isi = $dom->saveHTML();
    }
      $user = auth()->user()->id;
      $berita_tgl = Carbon::now();
      $berita = Berita::create(['berita_nama' => $request->berita_nama,'berita_isi' => $berita_isi,'berita_tanggal' => $berita_tgl,'berita_gambar' => $berita_gambar,'users_id' => $user]);
      return back()->with('msg','Berita Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $berita = Berita::find($id);
      return view('admin.berita.show',compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $berita = Berita::find($id);
      return view('admin.berita.edit',compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $detail = $request->input('berita_isi');
      $berita_nama = $request['berita_nama'];
      $berita_gambar=null;
      $berita_isi=null;
      if ($detail == !null) {
      libxml_use_internal_errors(true);
      $dom = new \DomDocument();
    $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $images = $dom->getElementsByTagName('img');
      if($images->length > 0 ){
      foreach($images as $k => $img){
      $img->parentNode->removeChild($img);
      $data = $img->getAttribute('src');

      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);

      $data = base64_decode($data);
      $created_name = time().$k.'.jpeg';
      $image_name= "/pengunjung/images/berita/" .$created_name ;
      Image::make($data)->resize(600, null, function ($constraint) {
                     $constraint->aspectRatio();
                 })->save(public_path($image_name));
      $img->removeAttribute('src');
      $img->setAttribute('src', $image_name);
      $berita_gambar = $created_name;
    }
    }
      $berita_isi = $dom->saveHTML();
    }
      // $user = auth()->user()->id;
      // $berita_tgl = Carbon::now();
      $berita = Berita::find($id);
      $berita->berita_nama = $berita_nama;
      if($berita_isi == !null){$berita->berita_isi = $berita_isi;}
      if ($berita_gambar == !null){
        $path = public_path() . '/pengunjung/images/berita/';
         File::delete($path. $berita->berita_gambar);
        $berita->berita_gambar = $berita_gambar;
      }
      $berita->update();

      return redirect('admin_berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        $path = public_path() . '/pengunjung/images/berita/';
         File::delete($path. $berita->berita_gambar);
        $berita->delete();
        return back();
    }
}
