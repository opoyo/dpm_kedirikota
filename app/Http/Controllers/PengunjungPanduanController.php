<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PanduanModel as Panduan;

class PengunjungPanduanController extends Controller
{

  public function index()
  {

    $panduan = Panduan::All();

    return view('pengunjung.unduhan.panduan', compact('panduan'));
  }

}
