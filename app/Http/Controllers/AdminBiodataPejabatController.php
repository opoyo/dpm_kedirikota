<?php

namespace App\Http\Controllers;

use App\BiodataPejabatModel as BiodataPejabat;
use Illuminate\Http\Request;
use Image;
use File;

class AdminBiodataPejabatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $biodata = BiodataPejabat::orderBy('created_at','DESC')->get();
        return view('admin.biodata_pejabat.index',compact('biodata'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.biodata_pejabat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if ($request->hasFile('biodata_pejabat_thumb')) {
      $filerequest = $request->file('biodata_pejabat_thumb');
      $biodata= new BiodataPejabat;
      $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
      Image::make($filerequest)->resize(250, null, function ($constraint) {
          $constraint->aspectRatio();
      })->save(public_path('/pengunjung/images/biodata/' . $filename));

      }
      $biodata->biodata_pejabat_nip = $request['biodata_pejabat_nip'];
      $biodata->biodata_pejabat_nama = $request['biodata_pejabat_nama'];
      $biodata->biodata_pejabat_jabatan = $request['biodata_pejabat_jabatan'];
      $biodata->biodata_pejabat_biodata_thumb = $filename;
      $biodata-> save();
      return back()->with('msg','Data Berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BiodataPejabatModel  $biodataPejabatModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $biodata = BiodataPejabat::find($id);
          return view('admin.biodata_pejabat.show',compact('biodata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BiodataPejabatModel  $biodataPejabatModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $biodata = BiodataPejabat::find($id);
        return view('admin.biodata_pejabat.edit',compact('biodata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BiodataPejabatModel  $biodataPejabatModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $biodata = BiodataPejabat::find($id);
      if ($request->hasFile('biodata_pejabat_thumb')) {
      $filerequest = $request->file('biodata_pejabat_thumb');
      $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
      Image::make($filerequest)->resize(250, null, function ($constraint) {
          $constraint->aspectRatio();
      })->save(public_path('/pengunjung/images/biodata/' . $filename));

      $biodata->biodata_pejabat_nip = $request['biodata_pejabat_nip'];
      $biodata->biodata_pejabat_nama = $request['biodata_pejabat_nama'];
      $biodata->biodata_pejabat_jabatan = $request['biodata_pejabat_jabatan'];
      $biodata->biodata_pejabat_biodata_thumb = $filename;

    }else {
      $biodata->biodata_pejabat_nip = $request['biodata_pejabat_nip'];
      $biodata->biodata_pejabat_nama = $request['biodata_pejabat_nama'];
      $biodata->biodata_pejabat_jabatan = $request['biodata_pejabat_jabatan'];
    }



      // $biodata = BiodataPejabat::find($id);
      // $biodata->biodata_pejabat_nip = $request['biodata_pejabat_nip'];
      // $biodata->biodata_pejabat_nama = $request['biodata_pejabat_nama'];
      // $biodata->biodata_pejabat_jabatan = $request['biodata_pejabat_jabatan'];
      $biodata-> update();
      return redirect('master_biodata_pejabat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BiodataPejabatModel  $biodataPejabatModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $biodata = BiodataPejabat::find($id);
      $path = public_path() . '/pengunjung/images/biodata/';
       File::delete($path. $biodata->biodata_pejabat_biodata_thumb);
      $biodata -> delete();
      return back();
    }

    public function listData()
    {
        $biodata = BiodataPejabat::orderBy('created_at', 'DESC')->get();
        $no = 0;
        $data = array();
        foreach ($biodata as $list) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->biodata_pejabat_nip;
            $row[] = $list->biodata_pejabat_nama;
            $row[] = $list->biodata_pejabat_jabatan;
            $row[] = '<a onclick="detailForm('.$list->biodata_pejabat_id.')" class="btn btn-success" data-toggle="tooltip" data-placement="botttom" title="Lihat Detail"  style="color:white;"><i class="fa  fa-eye"></i></a>
            <a onclick="editForm('.$list->biodata_pejabat_id.')" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Edit Data"  style="color:white;"><i class="fa  fa-edit"></i></a>
            <a onclick="deleteData('.$list->biodata_pejabat_id.')" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"></i></a>';
            $data[] = $row;

        }

        $output = array("data" => $data);
        return response()->json($output);

    }
}
