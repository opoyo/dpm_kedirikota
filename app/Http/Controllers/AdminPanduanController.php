<?php

namespace App\Http\Controllers;

use App\PanduanModel as Panduan;
use Illuminate\Http\Request;
use File;

class AdminPanduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $panduan = Panduan::select('panduan_id','panduan_judul','panduan_file')->get();
        return view('admin.panduan.index',compact('panduan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $messages = array(
        'panduan_file.required'   => 'File tidak boleh kosong.',
        'panduan_judul.required'  => 'Judul tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'panduan_file.*' => 'required|file|mimes:pdf,doc,docx|max:2048',
            'panduan_judul' => 'required',
      ],$messages);

      $user_id = auth()->user()->id;
      $panduan = new Panduan;
      $file = $request->file('panduan_file');
      $ext = $file->getClientOriginalExtension();
      $panduan_file = time().".".$ext;
      $file->move('public/files/pand',$panduan_file);
      $panduan->panduan_judul = $request->input('panduan_judul');
      $panduan->panduan_file = $panduan_file;
      $panduan->users_id = $user_id;
      $panduan->save();
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PanduanModel  $panduanModel
     * @return \Illuminate\Http\Response
     */
    public function show(PanduanModel $panduanModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PanduanModel  $panduanModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PanduanModel $panduanModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PanduanModel  $panduanModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PanduanModel $panduanModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PanduanModel  $panduanModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $panduan = Panduan::find($id);
      $path = public_path() . '/files/pand/';
       File::delete($path. $panduan->panduan_file);
      $panduan->delete();
      return back();

    }
}
