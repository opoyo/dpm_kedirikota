<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tupoksi;

class PengunjungTupoksiController extends Controller
{

  public function index()
  {
    $tupoksi = Tupoksi::find(1);
    return view('pengunjung.tentang.tupoksi',compact('tupoksi'));
  }

}
