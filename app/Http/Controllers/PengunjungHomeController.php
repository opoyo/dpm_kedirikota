<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BeritaModel;
use App\SlideShowModel;
use App\PopupModel;
use App\VideoModel;
use App\PopupCheckModel;
use DB;
use Auth;

class PengunjungHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = BeritaModel::orderBy('berita_id', 'DESC')->limit(6)->get();
        $slideshow = SlideShowModel::All();
        $popup = PopupModel::orderBy('popup_id','DESC')->get();
        $video = VideoModel::orderBy('video_id','DESC')->get();
        $popup_check = PopupCheckModel::find(1);

        return view("pengunjung.home", compact('berita', 'slideshow', 'popup','video','popup_check'));
        // dd($berita);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
