<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeraturanModel as Peraturan;

class PengunjungPeratuanController extends Controller
{
  public function index()
  {

    $peraturan = Peraturan::All();

    return view('pengunjung.unduhan.peraturan', compact('peraturan'));
  }
}
