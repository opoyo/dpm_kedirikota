<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengaduanModel as Pengaduan;
use Alert;

class PengunjungPengaduanController extends Controller
{
  public function index(){

    $pengaduan = Pengaduan::where([
                    ['pengaduan_status_balas', '=', '1'],
                    ['pengaduan_aktif', '=', '1'],
                ])->orderBy('pengaduan_id','DESC')->get();
    $success = "0";
    return view('pengunjung.layanan.pengaduan', compact('pengaduan', 'success'));

    // dd($pengaduan);
  }

  public function store(Request $request)
  {

    $pengaduan_nama = $request['pengaduan_nama'];
    $pengaduan_alamat = $request['pengaduan_alamat'];
    $pengaduan_kelurahan = $request['pengaduan_kelurahan'];
    $pengaduan_kecamatan = $request['pengaduan_kecamatan'];
    $pengaduan_email = $request['pengaduan_email'];
    $pengaduan_telp = $request['pengaduan_telp'];
    $pengaduan_data = $request['pengaduan_data'];

    $pengaduan = new Pengaduan;
    $pengaduan ->pengaduan_nama = $pengaduan_nama;
    $pengaduan ->pengaduan_alamat = $pengaduan_alamat.", Kel. ".$pengaduan_kelurahan.", Kec. ".$pengaduan_kecamatan;
    $pengaduan ->pengaduan_email = $pengaduan_email;
    $pengaduan ->pengaduan_telp = $pengaduan_telp;
    $pengaduan ->pengaduan_data = $pengaduan_data;
    $pengaduan ->pengaduan_status_balas ="2";
    $pengaduan ->pengaduan_aktif ="1";
    $pengaduan -> save();

    // return redirect('site_pengaduan');

    $pengaduan = Pengaduan::where([
                    ['pengaduan_status_balas', '=', '1'],
                    ['pengaduan_aktif', '=', '1'],
                ])->orderBy('pengaduan_id','DESC')->get();
    $success = "1";
    return view('pengunjung.layanan.pengaduan', compact('pengaduan', 'success'));

  }

}
