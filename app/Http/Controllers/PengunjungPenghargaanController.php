<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenghargaanModel as Penghargaan;

class PengunjungPenghargaanController extends Controller
{
  public function index(){

    $penghargaan = Penghargaan::All();

    return view('pengunjung.tentang.penghargaan', compact('penghargaan'));
  }
}
