<?php

namespace App\Http\Controllers;

use App\PeraturanModel as Peraturan;
use Illuminate\Http\Request;
use File;

class AdminPeraturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peraturan = Peraturan::select('peraturan_id','peraturan_file','peraturan_judul')->get();
        return view('admin.peraturan.index',compact('peraturan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = array(
        'peraturan_file.required'   => 'File tidak boleh kosong.',
        'peraturan_judul.required'  => 'Judul tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'peraturan_file.*' => 'required|file|mimes:pdf,doc,docx|max:2048',
            'peraturan_judul' => 'required',
      ],$messages);

      $user_id = auth()->user()->id;
      $peraturan = new Peraturan;
      $file = $request->file('peraturan_file');
      $ext = $file->getClientOriginalExtension();
      $peraturan_file = time().".".$ext;
      $file->move('public/files/per',$peraturan_file);
      $peraturan->peraturan_judul = $request->input('peraturan_judul');
      $peraturan->peraturan_file = $peraturan_file;
      $peraturan->users_id = $user_id;
      $peraturan->save();
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PeraturanModel  $peraturanModel
     * @return \Illuminate\Http\Response
     */
    public function show(PeraturanModel $peraturanModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PeraturanModel  $peraturanModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PeraturanModel $peraturanModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PeraturanModel  $peraturanModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PeraturanModel $peraturanModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PeraturanModel  $peraturanModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $peraturan = Peraturan::find($id);
      $path = public_path() . '/files/per/';
       File::delete($path. $peraturan->peraturan_file);
      $peraturan->delete();
      return back();
    }
}
