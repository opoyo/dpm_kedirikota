<?php

namespace App\Http\Controllers;

use App\InovasiLayananModel as InovasiLayanan;
use Image;
use Carbon\Carbon;
use File;

use Illuminate\Http\Request;

class AdminInovasiLayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $inovasi_layanan = InovasiLayanan::select('inovasi_layanan_nama','inovasi_layanan_id')->orderBy('created_at','DESC')->get();
      return view('admin.inovasi_layanan.index',compact('inovasi_layanan'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.inovasi_layanan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $detail=$request->input('inovasi_layanan_isi');
      $inovasi_layanan_gambar='';
      $inovasi_layanan_isi='';
      if ($detail == !null) {

        $dom = new \DomDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        foreach($images as $k => $img){
          $img->parentNode->removeChild($img);
          $data = $img->getAttribute('src');

          list($type, $data) = explode(';', $data);
          list(, $data)      = explode(',', $data);

          $data = base64_decode($data);
          $created_name = time().$k.'.jpeg';
          $image_name= "/pengunjung/images/inovasi_layanan/" .$created_name ;
          Image::make($data)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
          })->save(public_path($image_name));
          $img->removeAttribute('src');
          $img->setAttribute('src', $image_name);
          $inovasi_layanan_gambar = $created_name;
        }
        $inovasi_layanan_isi = $dom->saveHTML();
      }
      $inovasi_layanan = InovasiLayanan::create(['inovasi_layanan_nama' => $request->inovasi_layanan_nama,'inovasi_layanan_isi' => $inovasi_layanan_isi,'inovasi_layanan_gambar' => $inovasi_layanan_gambar]);
      return back()->with('msg','Data Inovasi Layanan Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $inovasi_layanan = InovasiLayanan::find($id);
      return view('admin.inovasi_layanan.show',compact('inovasi_layanan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $inovasi_layanan = InovasiLayanan::find($id);
      return view('admin.inovasi_layanan.edit',compact('inovasi_layanan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $detail = $request->input('inovasi_layanan_isi');
      $inovasi_layanan_nama = $request['inovasi_layanan_nama'];
      $inovasi_layanan_gambar=null;
      $inovasi_layanan_isi=null;
      if ($detail == !null) {
        libxml_use_internal_errors(true);
        $dom = new \DomDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        if($images->length > 0 ){
          foreach($images as $k => $img){
            $img->parentNode->removeChild($img);
            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $created_name = time().$k.'.jpeg';
            $image_name= "/pengunjung/images/inovasi_layanan/" .$created_name ;
            Image::make($data)->resize(600, null, function ($constraint) {
              $constraint->aspectRatio();
            })->save(public_path($image_name));
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
            $inovasi_layanan_gambar = $created_name;
          }
        }
        $inovasi_layanan_isi = $dom->saveHTML();
      }
      // $user = auth()->user()->id;
      // $berita_tgl = Carbon::now();
      $inovasi_layanan = InovasiLayanan::find($id);
      $inovasi_layanan->inovasi_layanan_nama = $inovasi_layanan_nama;
      if($inovasi_layanan_isi == !null){$inovasi_layanan->inovasi_layanan_isi = $inovasi_layanan_isi;}
      if ($inovasi_layanan_gambar == !null){
        $path = public_path() . '/pengunjung/images/inovasi_layanan/';
        File::delete($path. $inovasi_layanan->inovasi_layanan_gambar);
        $inovasi_layanan->inovasi_layanan_gambar = $inovasi_layanan_gambar;
      }
      $inovasi_layanan->update();

      return redirect('admin_inovasi_layanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $inovasi_layanan = InovasiLayanan::find($id);
      $path = public_path() . '/pengunjung/images/inovasi_layanan/';
       File::delete($path. $inovasi_layanan->inovasi_layanan_gambar);
      $inovasi_layanan->delete();
      return back();
    }
}
