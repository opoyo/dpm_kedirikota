<?php

namespace App\Http\Controllers;

use App\PotensiModel as Potensi;
use Illuminate\Http\Request;
use Image;
use Carbon\Carbon;
use File;

class AdminPotensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $potensi = Potensi::select('potensi_nama','potensi_id')->orderBy('created_at','DESC')->get();
      return view('admin.potensi.index',compact('potensi'))->with('no',1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.potensi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $detail=$request->input('potensi_isi');
      $potensi_gambar='';
      $potensi_isi='';
      if ($detail == !null) {

      $dom = new \DomDocument();
      $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $images = $dom->getElementsByTagName('img');
      foreach($images as $k => $img){
      $img->parentNode->removeChild($img);
      $data = $img->getAttribute('src');

      list($type, $data) = explode(';', $data);
      list(, $data)      = explode(',', $data);

      $data = base64_decode($data);
      $created_name = time().$k.'.jpeg';
      $image_name= "/pengunjung/images/potensi/" .$created_name ;
      Image::make($data)->resize(600, null, function ($constraint) {
                     $constraint->aspectRatio();
                 })->save(public_path($image_name));
      $img->removeAttribute('src');
      $img->setAttribute('src', $image_name);
      $potensi_gambar = $created_name;
    }
      $potensi_isi = $dom->saveHTML();
    }
      $potensi = Potensi::create(['potensi_nama' => $request->potensi_nama,'potensi_isi' => $potensi_isi,'potensi_gambar' => $potensi_gambar]);
      return back()->with('msg','Data Potensi Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $potensi = Potensi::find($id);
      return view('admin.potensi.show',compact('potensi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $potensi = Potensi::find($id);
      return view('admin.potensi.edit',compact('potensi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $detail = $request->input('potensi_isi');
      $potensi_nama = $request['potensi_nama'];
      $potensi_gambar=null;
      $potensi_isi=null;
      if ($detail == !null) {
        libxml_use_internal_errors(true);
        $dom = new \DomDocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        if($images->length > 0 ){
          foreach($images as $k => $img){
            $img->parentNode->removeChild($img);
            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $created_name = time().$k.'.jpeg';
            $image_name= "/pengunjung/images/potensi/" .$created_name ;
            Image::make($data)->resize(600, null, function ($constraint) {
              $constraint->aspectRatio();
            })->save(public_path($image_name));
            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
            $potensi_gambar = $created_name;
          }
        }
        $potensi_isi = $dom->saveHTML();
      }
      // $user = auth()->user()->id;
      // $berita_tgl = Carbon::now();
      $potensi = Potensi::find($id);
      $potensi->potensi_nama = $potensi_nama;
      if($potensi_isi == !null){$potensi->potensi_isi = $potensi_isi;}
      if ($potensi_gambar == !null){
        $path = public_path() . '/pengunjung/images/potensi/';
        File::delete($path. $potensi->potensi_gambar);
        $potensi->potensi_gambar = $potensi_gambar;
      }
      $potensi->update();

      return redirect('admin_potensi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $potensi = Potensi::find($id);
      $path = public_path() . '/pengunjung/images/potensi/';
       File::delete($path. $potensi->potensi_gambar);
      $potensi->delete();
      return back();
    }
}
