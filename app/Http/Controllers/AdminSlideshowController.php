<?php

namespace App\Http\Controllers;

use App\SlideshowModel as Slideshow;
use Illuminate\Http\Request;
use Image;
use File;

class AdminSlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slideshow = Slideshow::select('slideshow_id','slideshow_gambar')->orderBy('created_at','DESC')->get();
        $slideshowCount = Slideshow::count();
        return view('admin.slideshow.index',compact('slideshow','slideshowCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = array(
        'slideshow_gambar.required'  => 'Gambar tidak boleh kosong.',
        'mimes'                   => 'File yang dapat diunggah: :values.',
        'max'=> [
        'file'    => 'File tidak boleh lebih dari :max kilobytes.',
      ],
      );
      $this->validate($request, [
            'slideshow_gambar' => 'required|image|mimes:jpg,png,jpeg|max:2048',
      ],$messages);

      $slideshow = new Slideshow;

      if ($request->hasFile('slideshow_gambar')) {
        $filerequest = $request->file('slideshow_gambar');
        $filename = time() . uniqid() . '.' . $filerequest->getClientOriginalExtension();
        Image::make($filerequest)->resize(1351, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/slide/' . $filename));
        Image::make($filerequest)->resize(250, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('/pengunjung/images/slide/tumb/' . $filename));

        $slideshow->slideshow_gambar = $filename;
        $slideshow->save();
      }
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SlideshowModel  $slideshowModel
     * @return \Illuminate\Http\Response
     */
    public function show(SlideshowModel $slideshowModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SlideshowModel  $slideshowModel
     * @return \Illuminate\Http\Response
     */
    public function edit(SlideshowModel $slideshowModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SlideshowModel  $slideshowModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlideshowModel $slideshowModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SlideshowModel  $slideshowModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $slideshow = Slideshow::find($id);
      $path = public_path() . '/pengunjung/images/slide/';
       File::delete($path. $slideshow->slideshow_gambar);

       $path = public_path() . '/pengunjung/images/slide/tumb/';
        File::delete($path. $slideshow->slideshow_gambar);
        $slideshow->delete();
        return back();
    }
}
