<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use App\VideoModel;

class AdminVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $video = new VideoModel;
      $video ->video_nama = $request['video_nama'];
      $video ->video_url = $request['video_url'];
      $video -> save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $video = VideoModel::find($id);
      echo json_encode($video);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $video = VideoModel::find($id);
      $video ->video_nama = $request['video_nama'];
      $video ->video_url = $request['video_url'];
      $video -> update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $video = VideoModel::find($id);
      $video -> delete();
    }

    public function listData()
    {
        $video = VideoModel::orderBy('video_id', 'DESC')->get();
        $no = 0;
        $data = array();
        foreach ($video as $list) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->video_nama;
            $row[] = '<a target="_blank" href="'.$list->video_url.'">'.$list->video_url.'</a>';
            $row[] = '
            <a onclick="editForm('.$list->video_id.')" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Edit Data"  style="color:white;"><i class="fa  fa-edit"></i></a>';
            // $row[] = '
            // <a onclick="editForm('.$list->video_id.')" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Edit Data"  style="color:white;"><i class="fa  fa-edit"></i></a>
            // <a onclick="deleteData('.$list->video_id.')" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"></i></a>';
            $data[] = $row;

        }

        $output = array("data" => $data);
        return response()->json($output);

    }


}
