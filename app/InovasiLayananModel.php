<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InovasiLayananModel extends Model
{
  protected $table = 'inovasi_layanan';
  protected $primaryKey = 'inovasi_layanan_id';
  protected $fillable = ['inovasi_layanan_nama','inovasi_layanan_isi','inovasi_layanan_gambar'];
}
