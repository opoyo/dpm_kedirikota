<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeraturanModel extends Model
{
  protected $table = 'peraturan';
  protected $primaryKey = 'peraturan_id';
  protected $fillable = ['peraturan_judul','peraturan_file','users_id'];
}
