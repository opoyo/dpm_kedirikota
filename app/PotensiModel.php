<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotensiModel extends Model
{
  protected $table = 'potensi';
  protected $primaryKey = 'potensi_id';
  protected $fillable = ['potensi_nama','potensi_isi','potensi_gambar'];
}
