<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengaduanModel extends Model
{
  protected $table = 'pengaduan';
  protected $primaryKey = 'pengaduan_id';
}
