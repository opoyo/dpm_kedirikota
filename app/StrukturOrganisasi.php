<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StrukturOrganisasi extends Model
{
  protected $table = 'struktur_organisasi';
  protected $primaryKey = 'struktur_organisasi_id';
  protected $fillable = ['struktur_organisasi_gambar'];
}
