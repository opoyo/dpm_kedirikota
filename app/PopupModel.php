<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupModel extends Model
{
  protected $table = 'popup';
  protected $primaryKey = 'popup_id';
  protected $fillable = ['popup_nama','popup_gambar'];
}
