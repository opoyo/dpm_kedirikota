<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenghargaanModel extends Model
{
      protected $table = 'penghargaan';
      protected $primaryKey = 'penghargaan_id';
      protected $fillable = ['penghargaan_judul','penghargaan_gambar'];
}
