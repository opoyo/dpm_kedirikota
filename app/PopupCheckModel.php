<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopupCheckModel extends Model
{
  protected $table = 'popup_check';
  protected $primaryKey = 'popup_check_id';
  protected $fillable = ['popup_check_value'];
}
