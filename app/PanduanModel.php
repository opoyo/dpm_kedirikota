<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanduanModel extends Model
{
  protected $table = 'panduan';
  protected $primaryKey = 'panduan_id';
  protected $fillable = ['panduan_judul','panduan_file','users_id'];
}
