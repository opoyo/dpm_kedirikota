<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelayangPandang extends Model
{
    protected $table = 'selayang_pandang';
    protected $primaryKey = 'selayang_pandang_id';
    protected $fillable = ['selayang_pandang_isi'];
}
