<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tupoksi extends Model
{
  protected $table = 'tupoksi';
  protected $primaryKey = 'tupoksi_id';
  protected $fillable = ['tupoksi_isi'];
}
