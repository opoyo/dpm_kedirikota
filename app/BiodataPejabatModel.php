<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BiodataPejabatModel extends Model
{
  protected $table = 'biodata_pejabat';
  protected $primaryKey = 'biodata_pejabat_id';
  protected $fillable = ['biodata_pejabat_nip','biodata_pejabat_nama','biodata_pejabat_jabatan','biodata_pejabat_thumb
  '];
}
