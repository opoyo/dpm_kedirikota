<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleriModel extends Model
{
  protected $table = 'galeri';
  protected $primaryKey = 'galeri_id';
  protected $fillable = ['galeri_nama','galeri_gambar','galeri_id_parent'];
}
