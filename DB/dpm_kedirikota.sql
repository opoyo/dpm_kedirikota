/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.16-MariaDB : Database - dpm_kedirikota
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dpm_kedirikota` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dpm_kedirikota`;

/*Table structure for table `berita` */

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita` (
  `berita_id` int(11) NOT NULL AUTO_INCREMENT,
  `berita_nama` varchar(250) DEFAULT NULL,
  `berita_isi` text,
  `berita_tanggal` date DEFAULT NULL,
  `berita_gambar` text,
  `users_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`berita_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `berita` */

insert  into `berita`(`berita_id`,`berita_nama`,`berita_isi`,`berita_tanggal`,`berita_gambar`,`users_id`,`created_at`,`updated_at`) values 
(1,'DPMPTSP Kota Hadiri Rapat Kerja Rancangan KUA dan Rancangan PPAS','<p>Bertempat di Kantor DPMPTSP pada Senin, 2 Juli 2018 Kepala DPMPTSP, menerima kunjungan Ikatan Notaris Indonesia (INI) Kota . Rombongan INI berjumlah 7 orang dipimpin oleh Ketua INI Kota 2017-2020, Rosliah.<br><p>Sebelum melakukan pemaparan, memperkenalkan pejabat struktural dilingkungan DPMPTSP yang mendampinginya dalam menerima INI. Dalam pemaparanya, Kepala DPMPTSP menjelaskan tahapan-tahapan perizinan dan non perizinan serta dasar hukum pelayanan.&nbsp;</p><p>Dalam kesempatan tersebut, juga menjelaskan tentang progress layanan perizinan secara elektronik yang sudah berjalan di DPMPTSP. Tahapan pengembangan perizinan secara elektronik diharapkan dapat memberikan kemudahan pada layanan perizinan dan kemudahan berusaha.&nbsp;</p><p>\"Kami menyambut baik diskusi dengan Ikatan Notaris, mudah-mudahan ada masukan dan saran kepada kami untuk peningakatan layanan perizinan\", ungkap nya.&nbsp;</p><p>Lebih lanjut, Ketua INI dalam sambutan awalnya menyatakan bahwa pertemuan ini sangat bermanfaat untuk mendapatkan informasi secara utuh terkait layanan perizinan di Kota. \"kami berharap pertemuan seperti ini sering dilakukan untuk mendapat informasi kekinian\", ujar Rosliah.</p><p></p><p></p><p></p><p></p><p></p></p>\n','2019-08-27','15684417390.jpeg',1,'2019-09-14 13:15:39','2019-09-14 13:15:39'),
(2,'DPMPTSP Kota Hadiri Rapat Kerja Rancangan KUA dan Rancangan PPAS OE','<p>Bertempat di Kantor DPMPTSP pada Senin, 2 Juli 2018 Kepala DPMPTSP, menerima kunjungan Ikatan Notaris Indonesia (INI) Kota . Rombongan INI berjumlah 7 orang dipimpin oleh Ketua INI Kota 2017-2020, Rosliah.<p>Sebelum melakukan pemaparan, memperkenalkan pejabat struktural dilingkungan DPMPTSP yang mendampinginya dalam menerima INI. Dalam pemaparanya, Kepala DPMPTSP menjelaskan tahapan-tahapan perizinan dan non perizinan serta dasar hukum pelayanan.&Acirc;&nbsp;</p><p>Dalam kesempatan tersebut, juga menjelaskan tentang progress layanan perizinan secara elektronik yang sudah berjalan di DPMPTSP. Tahapan pengembangan perizinan secara elektronik diharapkan dapat memberikan kemudahan pada layanan perizinan dan kemudahan berusaha.&Acirc;&nbsp;</p><p>\"Kami menyambut baik diskusi dengan Ikatan Notaris, mudah-mudahan ada masukan dan saran kepada kami untuk peningakatan layanan perizinan\", ungkap nya.&Acirc;&nbsp;</p><p>Lebih lanjut, Ketua INI dalam sambutan awalnya menyatakan bahwa pertemuan ini sangat bermanfaat untuk mendapatkan informasi secara utuh terkait layanan perizinan di Kota. \"kami berharap pertemuan seperti ini sering dilakukan untuk mendapat informasi kekinian\", ujar Rosliah.</p></p>\n','2019-08-27','15684417640.jpeg',1,'2019-11-14 10:58:30','2019-11-14 10:58:30'),
(3,'DPMPTSP Dukung Kegiatan Penilaian Lomba Sinergitas Kinerja Kecamatan Tingkat Provinsi','<p><span style=\"color: rgb(119, 119, 119); font-family: Sintony, sans-serif; text-align: justify;\">Melalui pelayanan perizinan&nbsp; Mobil Keliling, DPMPTSP Kota ikut mendukung kegiatan penilaian lomba sinergitas kinerja Kecamatan Tingkat Provinsi Tahun 2018 yang di laksanakan pada hari Selasa tanggal 14 Agustus 2018 di Kecamatan dijelaskan oleh Kasi Sosialisasi dan Promosi DPMPTSP Kota kepada ketua tim penilai lomba bahwa mobling ini merupakan salah satu kerjasama antara DPMPTSP Kota dengan seluruh Kecamatan di Kota dalam upaya untuk mendekatkan&nbsp; akses pelayanan publik khususnya pelayanan perijinan kepada masyarakat sehingga masyarakat tidak perlu datang langsung ke kantor DPMPTSP Kota , kegiatan mobil keliling yang ditempatkan di Kantor Kecamatan ini memberikan pelayanan konsultasi, informasi serta membantu upload persyaratan pelayanan perizinan on line telah terjadwal selama satu tahun dengan berkeliling di masing masing kecamatan&nbsp; namun apabila pihak Kecamatan atau instansi lainnya membutuhkan pelayanan mobling&nbsp; di luar jadwal yang ada Mobling siap untuk memberikan pelayanan.D2</span><br></p>\n','2019-09-09','15684415730.jpeg',1,'2019-09-14 13:12:53','2019-09-14 13:12:53');

/*Table structure for table `biodata_pejabat` */

DROP TABLE IF EXISTS `biodata_pejabat`;

CREATE TABLE `biodata_pejabat` (
  `biodata_pejabat_id` int(11) NOT NULL AUTO_INCREMENT,
  `biodata_pejabat_nip` varchar(20) DEFAULT NULL,
  `biodata_pejabat_nama` varchar(200) DEFAULT NULL,
  `biodata_pejabat_jabatan` varchar(255) DEFAULT NULL,
  `biodata_pejabat_biodata_thumb` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`biodata_pejabat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `biodata_pejabat` */

insert  into `biodata_pejabat`(`biodata_pejabat_id`,`biodata_pejabat_nip`,`biodata_pejabat_nama`,`biodata_pejabat_jabatan`,`biodata_pejabat_biodata_thumb`,`created_at`,`updated_at`) values 
(3,'908087060','Bpk. Abcd','Kepala dinas','15755279685de8a62041b4a.png','2019-09-09 09:22:26','2019-12-05 13:39:30');

/*Table structure for table `galeri` */

DROP TABLE IF EXISTS `galeri`;

CREATE TABLE `galeri` (
  `galeri_id` int(11) NOT NULL AUTO_INCREMENT,
  `galeri_nama` varchar(200) DEFAULT NULL,
  `galeri_gambar` text,
  `galeri_id_parent` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`galeri_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `galeri` */

insert  into `galeri`(`galeri_id`,`galeri_nama`,`galeri_gambar`,`galeri_id_parent`,`created_at`,`updated_at`) values 
(1,'Galeri 1','15679966785d75bb065479f.jpg',0,'2019-12-06 20:24:56','0000-00-00 00:00:00'),
(6,'Galeri 3','15756426255dea6601098ed.jpg',0,'2019-12-06 21:30:25','2019-12-06 21:30:25'),
(9,'Galeri 3.2','15756436605dea6a0cdf785.jpg',6,'2019-12-06 21:47:41','2019-12-06 21:47:41'),
(10,'Galeri 1.2','15756436855dea6a25c8f01.jpg',1,'2019-12-06 21:48:05','2019-12-06 21:48:05'),
(11,'Galeri 3.3','15756878125deb1684354fc.jpg',6,'2019-12-07 10:03:34','2019-12-07 10:03:34');

/*Table structure for table `inovasi_layanan` */

DROP TABLE IF EXISTS `inovasi_layanan`;

CREATE TABLE `inovasi_layanan` (
  `inovasi_layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `inovasi_layanan_nama` varchar(200) DEFAULT NULL,
  `inovasi_layanan_isi` text,
  `inovasi_layanan_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`inovasi_layanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `inovasi_layanan` */

insert  into `inovasi_layanan`(`inovasi_layanan_id`,`inovasi_layanan_nama`,`inovasi_layanan_isi`,`inovasi_layanan_gambar`,`created_at`,`updated_at`) values 
(1,'inovasi 1','Dapatkan kemudahan dalam proses pelayanan perizinan melalui berbagai inovasi layanan','15755959550.jpeg','2020-01-27 12:53:11','0000-00-00 00:00:00');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `panduan` */

DROP TABLE IF EXISTS `panduan`;

CREATE TABLE `panduan` (
  `panduan_id` int(11) NOT NULL AUTO_INCREMENT,
  `panduan_judul` varchar(200) DEFAULT NULL,
  `panduan_file` text,
  `users_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`panduan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `panduan` */

insert  into `panduan`(`panduan_id`,`panduan_judul`,`panduan_file`,`users_id`,`created_at`,`updated_at`) values 
(1,'Panduan ... 2019','1567606277.pdf',NULL,'2019-09-06 09:54:55','0000-00-00 00:00:00'),
(2,'Panduan Kebijakan tahun 2020','1567606576.pdf',NULL,'2019-09-06 09:57:52','0000-00-00 00:00:00');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `pengaduan` */

DROP TABLE IF EXISTS `pengaduan`;

CREATE TABLE `pengaduan` (
  `pengaduan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengaduan_nama` varchar(200) DEFAULT NULL,
  `pengaduan_alamat` text,
  `pengaduan_telp` varchar(18) DEFAULT NULL,
  `pengaduan_email` varchar(200) DEFAULT NULL,
  `pengaduan_data` text,
  `pengaduan_balas` text,
  `pengaduan_status_balas` enum('1','2') DEFAULT NULL,
  `pengaduan_aktif` enum('1','2') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pengaduan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `pengaduan` */

insert  into `pengaduan`(`pengaduan_id`,`pengaduan_nama`,`pengaduan_alamat`,`pengaduan_telp`,`pengaduan_email`,`pengaduan_data`,`pengaduan_balas`,`pengaduan_status_balas`,`pengaduan_aktif`,`created_at`,`updated_at`) values 
(2,'HARIF SETYONO','Watulimo','085745291865','harif@gmail.com','selamat pagi, maaf mau tanya apa sekarang izin siup&TDP tidak bisa pararel? soalnya tadi sewaktu mau pengajuan izin pararel hanya ada untuk IMB,IPPTR,dan reklame','Yth. Bapak Ismet Musa.  Terima kasih atas masukan dan informasi yang telah disampaikan. Perihal Izin SIUP dan TDP yang bapak ajukan perizinan yang bapak ajukan mohon maaf tidak bisa paralel pak. Apabila memerlukan informasi lebih lanjut silahkan menghubungi call center kami','2','1','2019-11-14 11:19:51','0000-00-00 00:00:00'),
(6,'UNDUR MOH ADEN','Jl. Brawijaya No.40, Kel. Mojoroto, Kec. Mojoroto','098222999222','undur@gmail.com','yth. Mohon di mengerti dengan jelas isi dari penolakan pengajuan permohonan dengan isi (SILAHKAN DAFTAR DI OSS.GO.ID / SEMUA PERIZINAN HARUS MEMILIKI NIB (NOMOR INDUK BERUSAHA, padahal saya bisa masuk ke sistem periiznan online itu pake NIB, bagaimana bisa mengeluarkan penolakan saya harus daftar kembali ke OSS. Saya harus bagaimana selanjutnya, NIB sudah punya dengan nomor : 8120002931475, terima kasih','Yth. Bapak Undur Moh Aden. Terima kasih atas masukan dan informasi yang telah disampaikan. Perihal penolakan berkas. apabila memang dilokasi tidak ada produksi, silahkan perbaiki permohonannya dan silahkan Konfirmasi langsung ke DPMPTSP','1','1','2019-11-14 11:25:17','2019-11-14 11:25:17');

/*Table structure for table `penghargaan` */

DROP TABLE IF EXISTS `penghargaan`;

CREATE TABLE `penghargaan` (
  `penghargaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `penghargaan_judul` varchar(250) DEFAULT NULL,
  `penghargaan_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`penghargaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `penghargaan` */

insert  into `penghargaan`(`penghargaan_id`,`penghargaan_judul`,`penghargaan_gambar`,`created_at`,`updated_at`) values 
(3,'Penghargaan 1','15684407995d7c81df06762.jpg','2019-09-14 12:59:59','2019-09-14 12:59:59'),
(4,'Penghargaan 2','15684408165d7c81f0b7e64.jpg','2019-09-14 13:00:16','2019-09-14 13:00:16');

/*Table structure for table `peraturan` */

DROP TABLE IF EXISTS `peraturan`;

CREATE TABLE `peraturan` (
  `peraturan_id` int(11) NOT NULL AUTO_INCREMENT,
  `peraturan_judul` varchar(200) DEFAULT NULL,
  `peraturan_file` text,
  `users_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`peraturan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `peraturan` */

insert  into `peraturan`(`peraturan_id`,`peraturan_judul`,`peraturan_file`,`users_id`,`created_at`,`updated_at`) values 
(1,'Peraturan 1','1567671639.pdf',1,'2019-09-05 15:20:39','2019-09-05 15:20:39');

/*Table structure for table `popup` */

DROP TABLE IF EXISTS `popup`;

CREATE TABLE `popup` (
  `popup_id` int(11) NOT NULL AUTO_INCREMENT,
  `popup_nama` varchar(200) DEFAULT NULL,
  `popup_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`popup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `popup` */

insert  into `popup`(`popup_id`,`popup_nama`,`popup_gambar`,`created_at`,`updated_at`) values 
(6,'popup 2','15679966785d75bb065479f.jpg','2019-09-09 09:37:58','2019-09-09 09:37:58'),
(7,'popup 3','15679966915d75bb13bab1f.jpg','2019-09-09 09:38:11','2019-09-09 09:38:11');

/*Table structure for table `popup_check` */

DROP TABLE IF EXISTS `popup_check`;

CREATE TABLE `popup_check` (
  `popup_check_id` int(11) NOT NULL AUTO_INCREMENT,
  `popup_check_value` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`popup_check_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `popup_check` */

insert  into `popup_check`(`popup_check_id`,`popup_check_value`,`created_at`,`updated_at`) values 
(1,1,NULL,'2020-01-31 20:52:22');

/*Table structure for table `potensi` */

DROP TABLE IF EXISTS `potensi`;

CREATE TABLE `potensi` (
  `potensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `potensi_nama` varchar(200) DEFAULT NULL,
  `potensi_isi` text,
  `potensi_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`potensi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `potensi` */

insert  into `potensi`(`potensi_id`,`potensi_nama`,`potensi_isi`,`potensi_gambar`,`created_at`,`updated_at`) values 
(1,'Tahu takwa kediri','<p><span style=\'color: rgb(85, 85, 85);  font-size: 12px; letter-spacing: 0.5px;\'><br></span><p><span style=\'color: rgb(85, 85, 85); font-family: \"playfair display\"; font-size: 12px; letter-spacing: 0.5px;\'>Tahu adalah makanan yang di buat dari kacang kedelai yang di fermentasikan dan di ambil sarinya, berbeda dengan tempe yang asli dari indonesia sedangkan tahu berasal dari china. Tahu berasal dari kata Hokkian (tauhu) yang berarti kedelai yang di fermentasi. Tahu pertama kali di buat di Tiongkok</span><br></p></p>\n','15755959550.jpeg','2019-12-06 09:23:14','2019-12-06 08:32:35'),
(2,'Tenun Ikat Asli Kediri','<p><p><span style=\'color: rgb(85, 85, 85);  font-size: 12px; letter-spacing: 0.5px;\'>Tenun ikat adalah produk budaya yang tersebar di daerah Indonesia,setiap daerah memiliki ciri khas &nbsp;dan motif masing &acirc;&#128;&#147; masing, kesamaan tenun terdapat pada teknik pembuatannya yang menggunakan Alat Tenun bukan menggunakan mesin sehingga pembuatanya di perlukan tenaga manusia dan di butuhkan</span><br></p></p>\n','15755961270.jpeg','2019-12-06 09:23:02','2019-12-06 08:55:56');

/*Table structure for table `selayang_pandang` */

DROP TABLE IF EXISTS `selayang_pandang`;

CREATE TABLE `selayang_pandang` (
  `selayang_pandang_id` int(11) NOT NULL AUTO_INCREMENT,
  `selayang_pandang_isi` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`selayang_pandang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `selayang_pandang` */

insert  into `selayang_pandang`(`selayang_pandang_id`,`selayang_pandang_isi`,`created_at`,`updated_at`) values 
(1,'<p>Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPM PTSP) terbentuk berdasarkan Peraturan Daerah nomor ..................... tahun 2016, dan untuk pelaksanaan tugas berdasarkan Peraturan Walikota nomor 81 tahun 2016. Berdasarkan tugas pokok dan fungsinya, Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu merupakan salah satu instansi pemerintah yang menangani pelayanan secara langsung kepada masyarakat.<p>\r\n\r\nAdapaun dasar dari pelaksanaan penerbitan prosuk perizinan berdasarkan pelimpahan pendelegasian Kepala Daerah kepada Kantor Pelayanan Perizinan Terpadu (saat itu), berdasarkan Peraturan Walikota nomor 26 tahun 2012, dan perizinan yang dilimpahkan masih perizinan usaha dan izin pendukung dari usaha tersebut, dengan total izin yang telah ditangani sampai tahun 2017 ini adalah sebanyak 17 izin.</p><p>\r\n\r\nDalam upaya mendekatkan diri pada masyarakat dan membuka peluang investasi yang mendorong pertumbuhan ekonomi bagi masyarakat, maka Pemerintah Kota Kediri memandang perlu untuk,memberikan kemudahan pelayanan kepada masyarakat, dengan menggunakan media aplikasi yang terintegrasi dengan server jaringan sehingga lakukan perubahan terhadap system pelayanan perizinan dengan pola pelayanan satu pintu. Di bentuknya Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu ini dilatarbelakangi adanya kebutuhan masyarakat yang membutuhkan pelayanan publik yang berada pada satu pintu, maksudnya, hanya dengan menuju satu tempat, segala jenis perizinan yang dibutuhkan dapat dilayani, mulai dari pengambilan formulir, kelengkapan administrasi yang berkaitan dengan perizinan, loket pembayaran retribusi dan pajak serta pengambilan produk izin berada pada satu tempat.\r\n</p><p>\r\nParadigma bahwa Pemerintah adalah pelayan sedangkan masyarakat adalah pelanggan (customer) yang harus dilayani secara prima, yaitu dengan menghilangkan kesan prosedur yang berbelit-belit, persyaratan yang tidak jelas, biaya yang tidak transparan, waktu penyelesaian yang tidak pasti dan petugas yang tidak ramah.\r\n</p><p><b><u>\r\n \r\n\r\nHISTORI</u></b></p><p><b><br></b></p><p>\r\nTerbentuknya OPD Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu berdasarkan pada Peraturan Menteri Dalam Negeri nomor 20 tahun 2008, tentang Pedoman Organisasi dan Tata Kerja Unit Pelayanan Perizinan Terpadu di Daerah, maka dibentuklah Kantor Pelayanan Perizinan Terpadu, yang didasarkan pada Peraturan Daerah nomor 5 tahun 2008 tentang Organisasi Lembaga Teknis Kota Kediri, dan Peraturan Walikota nomor 38 tahun 2008 yang mengatur tentang Rincian Tugas Pokok dan Fungsi Kantor Pelayanan Terpadu Kota Kediri dan Jenis izin yang dilayani sebanyak 11 (sebelas) jenis izin yang diatur melalui Peraturan Walikota nomor 13 tahun 2008, tentang Pedoman dan Mekanisme Pelayanan Umum di Kantor Pelayanan Perijinan Terpadu Kota Kediri.</p><p>\r\n\r\nPada tahun 2009, tepatnya Peraturan Walikota Kediri nomor 15 tahun 2009, terdapat perubahan tentang Rincian Tugas Pokok dan Fungsi Kantor Pelayanan Perizinan Terpadu Kota Kediri, yang semula menangani 11 jenis perizinan, bertambah menjadi 34 (tiga puluh empat) jenis perizinan, dan. Regulasi pada Kantor Pelayanan Perizinan Terpadu masih mengalami perubahan, seiring dengan permintaan izin yang dibutuhkan oleh masyarakat. Sehingga, dalam rangka meningkatkan pelayanan kepada masyarakat, peraturan tersebut diubah melalui Peraturan Walikota nomor 14 tahun 2010 tentang Perubahan atas Peraturan Walikota nomor 13 tahun 2008 tentang Pedoman dan Mekanisme Pelayanan Umum di Kantor Pelayanan Perizinan Terpadu, dengan jumlah izin yang dilayani sebanyak 35 (tiga puluh lima) jenis izin.\r\n\r\nAtensi pemerintah daerah terhadap pelayanan yang berkualitas amat besar. </p><p>Karena masyarakat sebagai customer pemerintah tentunya mengharapkan mendapatkan terpenuhinya kebutuhannya secara cepat, tepat dan akurat. Disisi lain, pemerintah daerah juga membutuhkan masyarakat untuk berinvestasi sebanyak-banyaknya di daerahnya, agar pertumbuhan masyarakat juga dapat terus meningkat.\r\n\r\nUntuk itulah, disusunlah Peraturan Daerah nomor 5 tahun 2011 yang mengatur tentang Perubahan atas Peraturan Daerah Kota Kediri nomor 5 tahun 2008 tentang Organisasi Lembaga Teknis Kota Kediri. Pada Peraturan Daerah ini, dinyatakan bahwa Kantor Pelayanan Perizinan Terpadu Kota Kediri, tidak hanya melayani perizinan, namun dalam rangka pelaksanaan Peraturan Presiden nomor 27 tahun 2009 tentang Pelayanan Terpadu Satu Pintu, maka Kantor Pelayanan Perizinan Terpadu mempunyai tugas pokok melaksanakan koordinasi dan menyelenggarakan pelayanan administrasi di bidang perizinan dan penanaman modal secara terpadu, dengan prinsip koordinasi, integrasi, sinkronisasi, simplifikasi, keamanan dan kepastian.\r\n\r\nSebagai dasar pelaksanaan tugas, maka disusunlah Peraturan Walikota nomor 26 tahun 2012 tentang Rincian Tugas Pokok dan Fungsi Kantor Pelayanan Perizinan Terpadu Kota Kediri. Pada Peraturan Walikota ini, izin yang dilayani sebanyak 12 (dua belas) izin.\r\n</p><p>\r\nDan untuk lebih mengoptimalkan pelayanan kepada masyarakat, sebagaimana amanah dalam Peraturan Menteri Dalam Negeri nomor 16 tahun 2014, dan ditindaklanjuti dengan Peraturan Daerah nomor ......................... tahun 2016 dan Peraturan Walikota nomor 81 tahun 2016 untuk Rician Tugas Pokok dan Fungsi Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu, maka :</p><p></p><p></p></p>\n','2020-02-04 16:17:30','2020-02-04 16:17:30');

/*Table structure for table `slideshow` */

DROP TABLE IF EXISTS `slideshow`;

CREATE TABLE `slideshow` (
  `slideshow_id` int(11) NOT NULL AUTO_INCREMENT,
  `slideshow_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`slideshow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `slideshow` */

insert  into `slideshow`(`slideshow_id`,`slideshow_gambar`,`created_at`,`updated_at`) values 
(5,'15679964565d75ba28efdb0.PNG','2019-09-09 09:34:17','2019-09-09 09:34:17'),
(6,'15679964675d75ba338e556.jpg','2019-09-09 09:34:28','2019-09-09 09:34:28');

/*Table structure for table `struktur_organisasi` */

DROP TABLE IF EXISTS `struktur_organisasi`;

CREATE TABLE `struktur_organisasi` (
  `struktur_organisasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_organisasi_gambar` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`struktur_organisasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `struktur_organisasi` */

insert  into `struktur_organisasi`(`struktur_organisasi_id`,`struktur_organisasi_gambar`,`created_at`,`updated_at`) values 
(4,'15808073605e3934c071af1.png','2020-02-04 16:09:21','2020-02-04 16:09:21');

/*Table structure for table `tupoksi` */

DROP TABLE IF EXISTS `tupoksi`;

CREATE TABLE `tupoksi` (
  `tupoksi_id` int(11) NOT NULL AUTO_INCREMENT,
  `tupoksi_isi` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tupoksi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tupoksi` */

insert  into `tupoksi`(`tupoksi_id`,`tupoksi_isi`,`created_at`,`updated_at`) values 
(1,'<div>Tugas dari Dinas Penanaman Modal dan Perizinan Terpadu Satu Pintu adalah :<div><br></div><div>a. Tugas A</div><div>b. Tugas B</div><div>c.</div><div><br></div><div><br></div><div>Sedangkan fungsi Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu adalah :</div><div><br></div><div>a. Penyusunan program Kantor Pelayanan Perizinan Terpadu</div><div>b. Penyelenggaraan Perizinan dan Penanaman Modal</div><div>c. Pelaksanaan koordinasi proses pelayanan perizinan dan Penanaman Modal</div><div>d. Pelaksanaan administrasi pelayanan perizinan dan Penanaman Modal</div><div>e. Pemantauan dan evaluasi penyelenggaraan pelayanan Perizinan dan Penananam Modal</div><div>f. Pelaksanaan tugas lain yang diberikan Atasan Langsungnya sesuai dengan tugas pokok dan fungsinya.</div></div>\n','2020-02-04 16:21:23','2020-02-04 16:21:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'harif s','harif','$2y$10$.DUCv48vi4kdMwlELWmEae3.5n.Bu6F5rvzi50KM11fUbLpQuy3tO','hnzWBkvx1wwKyYV9S3KvAvEZwOz1yS5EyQe8HwwMJgS8xxmbDCwE0ROl3oJC',NULL,'2020-02-04 14:29:41'),
(3,'setyono','setyono','$2y$10$BrYan3WfJGLbgyjhc.KH6uxVNQOzNtrJOnqEn.Pcr55ExXXxYNSs2','Rmm0qD5rxGro4FmK4aBcvoATEENH2wA8iGYz4PWjeCmh1E59lF8dm7lRE8n8','2019-09-02 11:34:13','2019-09-02 11:36:45');

/*Table structure for table `video` */

DROP TABLE IF EXISTS `video`;

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `video_nama` varchar(200) DEFAULT NULL,
  `video_url` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `video` */

insert  into `video`(`video_id`,`video_nama`,`video_url`,`created_at`,`updated_at`) values 
(1,'OSS KSWI Kota Kediri 2019','https://www.youtube.com/embed/CqNMIgy_u0E','2019-12-05 13:03:25','0000-00-00 00:00:00'),
(2,'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU KOTA KEDIRI','https://www.youtube.com/embed/ZPORe0zEJM0','2019-12-05 13:32:13','2019-12-05 13:32:13');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
