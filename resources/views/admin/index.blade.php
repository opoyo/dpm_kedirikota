<?php $hal = "index"; ?>
@extends('layouts.admin.master')
@section('title', 'Admin-Index')

@section('css')
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Beranda
    <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- ==================================================1================================================= -->
  <div class="row">


    <a href="{{url('admin_video')}}">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-youtube-play"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Video</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </a>

    <a href="{{url('admin_potensi')}}">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-star-half-full"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Potensi</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <a/>




      <a href="{{url('master_biodata_pejabat')}}">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-address-book"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Biodata Pejabat</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>
      <a href="{{url('admin_panduan_aplikasi')}}">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-black-tie"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Panduan Aplikasi</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>

    </div>
    <!-- =================================================================================================== -->

    <!-- ==================================================2================================================= -->
    <div class="row">

      <a href="{{url('admin_panduan')}}">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Panduan</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>

      <a href="{{url('admin_peraturan')}}">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-file-text-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Peraturan</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>

      <a href="{{url('admin_penghargaan')}}">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-address-card-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Penghargaan</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <a/>

        <a href="{{url('master_user')}}">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-user-circle-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Master User</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <a/>

        </div>
        <!-- =================================================================================================== -->

        <!-- ==================================================3================================================= -->
        <div class="row">

          <a href="{{url('admin_berita')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-newspaper-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Berita</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>
          <a href="{{url('admin_slideshow')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-film"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Slideshow</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>

          <a href="{{url('admin_popup')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa  fa-map-pin"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Popup</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>
          <a href="{{url('admin_galeri')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-image"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Galeri</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>



        </div>
        <!-- =================================================================================================== -->


        <!-- ==================================================4================================================= -->
        <div class="row">
          <a href="{{url('admin_inovasi_layanan')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-bus"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Inovasi Layanan</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>


          <a href="{{url('admin_selayang_pandang')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-diamond"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Selayang Pandang</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>



          <a href="{{url('admin_tupoksi')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa  fa-hand-rock-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Tugas Pokok & Fungsi</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>



          <a href="{{url('admin_struktur_organisasi')}}">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa  fa-map"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Struktur Organisasi</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
          </a>
        </div>
        <!-- =================================================================================================== -->



      </section>
      <!-- /.content -->
      @endsection


      @section('js')
      @endsection
