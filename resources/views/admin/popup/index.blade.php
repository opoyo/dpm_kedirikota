<?php $hal = "admin_popup"; ?>
@extends('layouts.admin.master')
@section('title', 'Popup')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }

  .example-modal .modal {
    background: transparent !important;
  }

  /* .btn-delete-red{
    background-color:rgba(255, 0, 0, 0.5);
    color:#000;
    font-size:100%;
  }
  /* a.btn-delete-red:hover {
    background:#ffffff;
    background:-moz-linear-gradient(top,  #ffffff 0%, #f6f6f6 47%, #ededed 100%);
    background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color- stop(47%,#f6f6f6), color-stop(100%,#ededed));
    background:-webkit-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:-o-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:-ms-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:linear-gradient(to bottom,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 );color:#266CAE}
} */
</style>
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Popup
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Popup</h3>
        </div>
        @if ($popupCount >= 3)
        <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-warning"></i> Peringatan!</h4>
              Maksimal Gambar 3
            </div>
        @else
         @if (count($errors) > 0)
          <ul style="color:red;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom:20px;margin-left:10px;" data-target="#modal-form"><i class="fa  fa-plus-square-o"></i> Tambah</button>

        @foreach($popup_check as $chek)
        <input style="margin-left:10px" type="checkbox" name="popup_check_value" id="popup_check_value"
          @if ($chek->popup_check_value == '1')
            checked
          @endif
          value="1"> Cengtang untuk mengaktifkan Pop-up / Hilangkan centang untuk Menonaktifkan Pop-up
        @endforeach
        <br>
          <b style="margin-left:10px;color:red;">Note : </b>  <span style="" >Ukuran dimensi gambar  560x560 pixel  (max 2 Mb) </span>
          <br>



        @endif

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            @foreach ($popup as $data)

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="box box-danger" style="background:#f7f8fa;">
                <div class="box-header with-border">
                  {{-- <h3 class="box-title">{{$data->penghargaan_judul}}</h3> --}}
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <img src="{{asset('public/pengunjung/images/popup/tumb/'.$data->popup_gambar)}}" alt="">
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <form id="form1" onclick="return confirm('apakah anda yakin?')" action="{{route('admin_popup.destroy',$data->popup_id)}}" method="post">
                      {{csrf_field()}}
                      {{method_field("DELETE")}}
                    <button class="uppercase btn btn-danger"  type="submit" name="button">Hapus</button>
                </form>
                </div>
                <!-- /.box-footer -->
              </div>
            </div>
            @endforeach
            <!-- /.col -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@include('admin.popup.form')

@endsection


@section('js')

<script type="text/javascript">
  function ConfirmDelete() {
    var x = confirm("Are you sure you want to delete?");
    if (x)
      return true;
    else
      return false;
  }



  $(document).ready(function(){
    $("input:checkbox").change(function() {
      var popup_check_id = 1;
      // console.log(program_id);
      $.ajax({
              type:'POST',
              url:'admin_popup/update_aktif',
              headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
              data: { "id" : popup_check_id },
              success: function(data){
                if(data.data.success){
                  // console.log(data);
                }
              }
          });
      });
  });
</script>




@endsection
