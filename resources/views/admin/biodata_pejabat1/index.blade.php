<?php $hal = "admin_biodata_pejabat"; ?>
@extends('layouts.admin.master')
@section('title', 'Biodata Pejabat')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }

  .example-modal .modal {
    background: transparent !important;
  }
</style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Biodata Pejabat
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Master Biodata Pejabat</h3>
        </div>
        <a onclick="addForm()" style="margin-bottom:20px;margin-left:10px;" class="card-body-title"><button class="btn btn-primary"><i class="fa  fa-plus-square-o"></i> Tambah</button></a>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="datatable1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width:5%">No #</th>
                <th style="width:16%">NIP </th>
                <th style="width:16%;">Nama</th>
                <th style="width:16%;">Jabatan</th>
                <th style="width:15%">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@include('admin.biodata_pejabat.form')
@endsection


@section('js')
<!-- DataTables -->
<script src="{{asset('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<script type="text/javascript">
  var table, save_method;
  $(function() {
    table = $('.table').DataTable({
      "processing": true,
      "ajax": {
        "url": "{{ route('data_master_biodata_pejabat') }}",
        "type": "GET"
      }
    });
    $('#modal-form').on('submit', function(e) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      if (!e.isDefaultPrevented()) {
        var id = $('#id').val();

        if (save_method == "add") url = "{{ route('master_biodata_pejabat.store') }}";
        else url = "master_biodata_pejabat/" + id;
        var formData = new FormData("#modal-form form");
        $.ajax({
          type: 'POST',
          url: url,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {
            // $('#modal-form').modal('hide');
            console.log(data);
            table.ajax.reload();
          },
          error: function() {
            alert("Tidak dapat menyimpan data!");
          }
        });
        return false;
      }
    });
  });

  function addForm() {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();
    $('.modal-title').text('Tambah Data Biodata Pejabat');
    document.getElementById("thumb-detail").src = "";

  }

  function detailForm(id) {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url: "master_biodata_pejabat/" + id + "/edit",
      type: "GET",
      dataType: "JSON",
      success: function(data) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Data Biodata Pejabat');
        $('#id').val(data.biodata_pejabat_id);
        $('#biodata_pejabat_nip').val(data.biodata_pejabat_nip);
        $('#biodata_pejabat_nama').val(data.biodata_pejabat_nama);
        $('#biodata_pejabat_jabatan').val(data.biodata_pejabat_jabatan);

        var elem = document.getElementById('biodata_pejabat_thumb');
        elem.parentNode.removeChild(elem);
        var thumb = data.biodata_pejabat_thumb;

        document.getElementById("thumb-detail").src = "public/pungunjung/images/biodata/" + thumb;
      },
      error: function() {
        alert("Tidak dapat menampilkan data !!!");
      }
    });
  }

  function editForm(id) {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url: "master_biodata_pejabat/" + id + "/edit",
      type: "GET",
      dataType: "JSON",
      success: function(data) {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Data Biodata Pejabat');
        $('#id').val(data.biodata_pejabat_id);
        $('#biodata_pejabat_nip').val(data.biodata_pejabat_nip);
        $('#biodata_pejabat_nama').val(data.biodata_pejabat_nama);
        $('#biodata_pejabat_jabatan').val(data.biodata_pejabat_jabatan);
        $('#biodata_pejabat_thumb').val(data.biodata_pejabat_thumb);
        document.getElementById("thumb-detail").src = "";

      },
      error: function() {
        alert("Tidak dapat menampilkan data !!!");
      }
    });
  }

  function deleteData(id) {
    if (confirm("Apakah yakin data akan dihapus?")) {
      $.ajax({
        url: "master_biodata_pejabat/" + id,
        type: "POST",
        data: {
          '_method': 'DELETE',
          '_token': $('meta[name=csrf-token]').attr('content')
        },
        success: function(data) {
          table.ajax.reload();
        },
        error: function() {
          alert("Tidak dapat menghapus data!");
        }
      });
    }
  }
</script>

<script>
  $(function() {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging': true,
      'lengthChange': false,
      'searching': false,
      'ordering': true,
      'info': true,
      'autoWidth': false
    })
  })
</script>


<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2({
      dropdownParent: $(".modal")
    });
  });

  $(' #confirm_password').on('keyup', function() {
    if ($('#password').val() == $('#confirm_password').val()) {
      $('#message').html('').css('color', 'green');
    } else
      $('#message').html('Password tidak cocok').css('color', 'red');
  });


  $('#confirm_password').keyup(function() {
    var pass = $('#password').val();
    var cpass = $('#confirm_password').val();
    if (pass != cpass) {
      $('#submit').attr({
        disabled: true
      });
    } else {
      $('#submit').attr({
        disabled: false
      });
    }
  });
</script>


@endsection
