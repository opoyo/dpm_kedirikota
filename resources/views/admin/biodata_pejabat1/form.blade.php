<div class="modal" id="modal-form"  role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Default Modal</h4>
      </div>

      <form id="modal-form" action="javascript:void(0)" enctype="multipart/form-data">
<div class="file-field">
<div class="row">
<div class=" col-md-8 mb-4">
<img id="original" src="" class=" z-depth-1-half avatar-pic" alt="">
<div class="d-flex justify-content-center mt-3">
<div class="btn btn-mdb-color btn-rounded float-left">
<input type="file" name="photo_name" id="photo_name" required=""> <br>
<button type="submit" class="btn btn-secondary d-flex justify-content-center mt-3">submit</button>
</div>
</div>
</div>
<div class=" col-md-4 mb-4">
<img id="thumbImg" src="" class=" z-depth-1-half thumb-pic"
alt="">
</div>
</div>
</form>
      {{-- <form method="post" id="upload_form" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('POST') }}
        <div class="modal-body">
          <input type="hidden" id="id" name="id">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">NIP</label>
            <div class="input-group col-sm-8">
              <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
              <input type="text" class="form-control" required name="biodata_pejabat_nip" id="biodata_pejabat_nip" placeholder="NIP">
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <!-- /.form-group -->



          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
            <div class="input-group col-sm-8">
              <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
              <input type="text" class="form-control" required name="biodata_pejabat_nama" id="biodata_pejabat_nama" placeholder="Nama">
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>

            <div class="input-group col-sm-8">
              <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
              <input type="text"  required class="form-control" name="biodata_pejabat_jabatan" id="biodata_pejabat_jabatan" placeholder="Jabatan">
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="col-sm-2 control-label">Photo</label>

            <div class="input-group col-sm-8">
              <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
              <input type="file" class="form-control" name="biodata_pejabat_thumb" id="biodata_pejabat_thumb" >
              <img src=""  id="thumb-detail" width="250" alt="">
            </div>
          </div>
          <!-- /.form-group -->




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning pull-left" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
          <button type="submit" id="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
        </div>
    </div>

    </form> --}}

    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
