<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Unggah Galeri</h4>
        </div>
        <form class="form-horizontal" action="{{route('admin_galeri.store')}}" data-toggle="validator" method="post" enctype="multipart/form-data">
          {{ csrf_field() }} {{ method_field('POST') }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Judul</label>
              <div class="input-group col-sm-8">
                <input type="text" class="form-control" name="galeri_nama" id="galeri_nama" placeholder="Nama Galeri">
                <!-- <span class="help-block with-errors"></span> -->
              </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label class="col-sm-2 control-label">Parent</label>
              <div class="input-group col-sm-8">
                <!-- <span class="input-group-addon"><i class="fa  fa-user"></i></span> -->
                <select  id="galeri_id_parent" name="galeri_id_parent" class="form-control js-example-basic-single" style="width: 100%;">
                  <option value="0" >--</option>
                  @foreach($combo as $list)
                  <option  value="{{ $list->galeri_id }}"> {{ $list->galeri_nama }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Gambar</label>
              <div class="input-group col-sm-8">
                <input type="file" class="form-control" required name="galeri_gambar" id="galeri_gambar">
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <!-- /.form-group -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning pull-left" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
            <button type="submit" id="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
          </div>
        </div>

      </form>

      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
