<?php $hal = "admin_galeri"; ?>
@extends('layouts.admin.master')
@section('title', 'Galeri')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
.example-modal .modal {
  position: relative;
  top: auto;
  bottom: auto;
  right: auto;
  left: auto;
  display: block;
  z-index: 1;
}

.example-modal .modal {
  background: transparent !important;
}

/* .btn-delete-red{
background-color:rgba(255, 0, 0, 0.5);
color:#000;
font-size:100%;
}
/* a.btn-delete-red:hover {
background:#ffffff;
background:-moz-linear-gradient(top,  #ffffff 0%, #f6f6f6 47%, #ededed 100%);
background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color- stop(47%,#f6f6f6), color-stop(100%,#ededed));
background:-webkit-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
background:-o-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
background:-ms-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
background:linear-gradient(to bottom,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 );color:#266CAE}
} */
</style>
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Galeri
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-header">
          <!-- <h3 class="box-title">Data Galeri</h3> -->
        </div>
        @if (count($errors) > 0)
          <ul style="color:red;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif
        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom:20px;margin-left:10px;" data-target="#modal-form"><i class="fa  fa-plus-square-o"></i> Tambah</button>
         <b style="margin-left:10px;color:red;">Note : </b>  <span style="" > Jika anda menghapus gambar yang berada di paling kiri (berwarna merah) berarti anda menghapus semua gambar yang berada di kanannya </span>
      </div>
      <?php $no = 1; ?>
      @foreach ($galeri as $data)
      <div class="box">

        <div class="box-header">
          <h5 class="box-title">{{$no}}. {{$data->galeri_nama}}</h5>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">


            <!-- start -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="box box-danger" style="background:#f7f8fa;">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <img src="{{asset('public/pengunjung/images/galeri/tumb/'.$data->galeri_gambar)}}" alt="">
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <form id="form1" onclick="return confirm('apakah anda yakin?')" action="{{route('admin_galeri.destroy',$data->galeri_id)}}" method="post">
                    {{csrf_field()}}
                    {{method_field("DELETE")}}
                    <button class="uppercase btn btn-danger"  type="submit" name="button">Hapus</button>
                  </form>
                </div>
                <!-- /.box-footer -->
              </div>
            </div>

            <!-- End -->

            <!-- start -->
            @foreach(App\GaleriModel::where('galeri_id_parent','=',$data->galeri_id)->get() as $ItemList)
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="box box-success" style="background:#f7f8fa;">
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <img src="{{asset('public/pengunjung/images/galeri/tumb/'.$ItemList->galeri_gambar)}}" alt="">
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <form id="form1" onclick="return confirm('apakah anda yakin?')" action="{{route('admin_galeri.destroy',$ItemList->galeri_id)}}" method="post">
                    {{csrf_field()}}
                    {{method_field("DELETE")}}
                    <button class="uppercase btn btn-danger"  type="submit" name="button">Hapus</button>
                  </form>
                </div>
                <!-- /.box-footer -->
              </div>
            </div>
            @endforeach
            <!-- End -->



            <!-- /.col -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <?php $no++; ?>
      @endforeach
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@include('admin.galeri.form')

@endsection


@section('js')
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
function ConfirmDelete() {
  var x = confirm("Are you sure you want to delete?");
  if (x)
  return true;
  else
  return false;
}
</script>

<script>
  // $(function () {
  //   //Initialize Select2 Elements
  //   $('.select2').select2()
  // })
  $(document).ready(function() {
  $('.js-example-basic-single').select2({
    dropdownParent: $(".modal")
  });
});
</script>


@endsection
