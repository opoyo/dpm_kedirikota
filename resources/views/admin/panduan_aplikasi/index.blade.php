<?php $hal = "admin_panduan_aplikasi"; ?>
@extends('layouts.admin.master')
@section('title', 'Admin-Panduan Penggunaan Aplikasi')

@section('css')
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Panduan Penggunaan Aplikasi
    <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">




  <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">DPM KOTA KEDIRI</h3>

      <div class="box-tools pull-right">
        <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button> -->

        <a target="_blank" href="{{asset('public/panduan_aplikasi/Panduan aplikasi.pdf')}}" class="btn btn-primary">Download Panduan (.pdf)</a>
        <br>

      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->


        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="" aria-expanded="true">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  A. Halaman Login
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Form Login</h3>
                    <div class="timeline-body">
                      Untuk mengakses halaman admin pertama akses link <a target="_blank" href="http://dpm.kedirikota.go.id/dpm_kedirikota/index_admin.html">http://dpm.kedirikota.go.id/dpm_kedirikota/index_admin.html</a>  kemudian masukkan username dan password yang valid.
                      <br>
                      <p style="text-align:center"><img style="width:40%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/1. Login/1. Isi login form.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->
        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  B.	Menu Berita
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">

                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List berita</h3>
                    <div class="timeline-body">
                      Menu berita digunakan untuk mengatur konten berita yang ditampilkan pada halaman pengunjung. Pada menu ini admin dapat (1) menambah data berita, (2) melihat detail berita, (3) Mengedit data berita, (4) dan menghapus data berita.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/2. Berita/1. list berita.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Tambah data berita</h3>
                    <div class="timeline-body">
                      Terdapat dua field yang harus di isi yaitu (1) Judul berita, dan (2) Isi berita. Pada field  isi berita admin harus memasukkan satu gambar dan deskripsi.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/2. Berita/2. tambah data berita.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Lihat detail berita</h3>
                    <div class="timeline-body">
                      Admin dapat melihat detail per-berita pada halaman ini.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/2. Berita/3. Lihat detail berita.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>4</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Edit data berita</h3>
                    <div class="timeline-body">
                      Pada halaman ini admin dapat melakukan edit/update judul dan isi berita. Jika ingin mengubah gambar maka pilih ikon picture dan masukan gambar baru, dan jika tidak ingin mengubah gambar maka tidak perlu memilih gambar lagi.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/2. Berita/4. Edit data berita.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>5</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Hapus data berita</h3>
                    <div class="timeline-body">
                      Untuk menghapus data berita maka admin cukup menkan tombol berwarna merah seperti gambar dibawah.
                      <br>
                      <p style="text-align:center"><img style="width:15%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/2. Berita/5. Hapus data berita.png')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  C. Menu Video
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseTwelve" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List Video</h3>
                    <div class="timeline-body">
                      Menu ini digunakan untuk mengelola data video yang ditampilkan pada halaman “home” yang diakses oleh pengunjung. Pada menu ini yang dapat dilakukan admin adalah melihat daftar video berupa link yang mengarah ke youtube dan admin dapat melakukan update link tersebut untuk mengganti video.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/12. Video/1. List video.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Update data video</h3>
                    <div class="timeline-body">
                      Terdapat dua field yang bisa dilakukan update yaitu nama video dan url video youtube.
                      <br>
                      <p style="text-align:center"><img style="width:70%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/12. Video/2. Update data video.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>




        <!-- ============================================================================================ -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  D.	Menu Potensi
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseThirteen" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">

                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List Potensi</h3>
                    <div class="timeline-body">
                      Menu potensi digunakan untuk mengatur konten potensi yang ditampilkan pada halaman pengunjung. Pada menu ini admin dapat (1) menambah data potensi, (2) melihat detail potensi, (3) Mengedit data potensi, (4) dan menghapus data potensi.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/13. Potensi/1. List Potensi.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Tambah data potensi</h3>
                    <div class="timeline-body">
                      Terdapat dua field yang harus di isi yaitu (1) Judul potensi, dan (2) Isi. Pada field isi admin harus memasukkan satu gambar dan deskripsi.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/13. Potensi/2. Tambah data potensi.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Lihat detail potensi</h3>
                    <div class="timeline-body">
                      Admin dapat melihat detail per-berita pada halaman ini.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/13. Potensi/3. Lihat detail potensi.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>4</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Edit data potensi</h3>
                    <div class="timeline-body">
                      Pada halaman ini admin dapat melakukan edit/update judul dan isi. Jika ingin mengubah gambar maka pilih ikon picture dan masukan gambar baru, dan jika tidak ingin mengubah gambar maka tidak perlu memilih gambar lagi.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/13. Potensi/4. Edit data potensi.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>5</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Hapus data potensi</h3>
                    <div class="timeline-body">
                      Untuk menghapus data potensi maka admin cukup menkan tombol berwarna merah seperti gambar dibawah.
                      <br>
                      <p style="text-align:center"><img style="width:15%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/13. Potensi/5. Hapus data potensi.png')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  C. Menu Galeri
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseFourteen" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Dashboard Galeri</h3>
                    <div class="timeline-body">
                      <p>Menu ini bersifat child dan parent (penjelasan lebih lengkap pada poin 2. Unggah gambar). Seperti tampilan dibawah jika gambar berwarna merah maka gambar tersebut adalah parent, apabila anda menghapus parent maka gambar berwarna hijau (child) yang berada di kanannya akan ikut terhapus juga. Dan jika anda hanya menghapus satu gambar child maka hanya gambar tersebut yang terhapus dan tidak akan berpengaruh pada gambar yang lain.</p>
                      <p>Note : Parent/merah (Gambar utama), Child/Hijau (Gambar pendukung yang berada di belakang parent)</p>
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/14. Galeri/1. Dashboard Galeri.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Unggah gambar</h3>
                    <div class="timeline-body">
                      Terdapat tiga field yang harus diisi, penjelasan :
                      <ol type="a">
                        <li>Judul : berisi judul galeri, untuk parent judul wajib diisi dan untuk child boleh diisi ataupun tidak diisi.</li>
                        <li>Parent : untuk menentukan gambar tersebut merupakan parent atau child. Untuk parent pilih silbol “--”, untuk child buka combo box dan pilih nama parent.</li>
                        <li>Gambar : pilih gambar yang hendak anda unggah.</li>
                      </ol>
                      <br>
                      <p style="text-align:center"><img style="width:60%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/14. Galeri/2. Unggah gambar.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>




        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  F. Menu Panduan
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List Panduan</h3>
                    <div class="timeline-body">
                      Menu panduan digunakan untuk mengatur konten panduan yang ditampilkan pada halaman pengunjung. Pada halaman ini berisi dokumen panduan penyelenggaraan program-program yang dilakukan oleh Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota Kediri.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/3. Panduan/1. List panduan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Mengunggah Dokumen Panduan</h3>
                    <div class="timeline-body">
                      Untuk Mengunggah dokumen tekan tombol “tambah”, kemudian muncul form isian lalu isikan nama dokumen dan pilih dokumen yang ingin diunggah. Jika sudah tekan tombol “simpan”.
                      <br>
                      <p style="text-align:center"><img style="width:70%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/3. Panduan/2. Mengunggah dokumen panduan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">3.	Mengunduh atau menghapus file panduan</h3>
                    <div class="timeline-body">
                      Untuk mengunduh dokumen klik tombol berwarna biru, dan untuk menghapus dokumen klik tombol berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:20%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/3. Panduan/3. Mengunduh atau menghapus file panduan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>




        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  G. Menu Peraturan
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List Peratuan</h3>
                    <div class="timeline-body">
                      Menu peraturan digunakan untuk mengatur konten peratuaran yang ditampilkan pada halaman pengunjung. Pada halaman ini berisi dokumen peraturan tentang penyelenggaraan program-program yang dilakukan oleh Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota Kediri.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/4. Peraturan/1. List peraturan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Mengunggah dokumen peraturan</h3>
                    <div class="timeline-body">
                      Untuk Mengunggah dokumen tekan tombol “tambah”, kemudian muncul form isian lalu isikan nama dokumen dan pilih dokumen yang ingin diunggah. Jika sudah tekan tombol “simpan”.
                      <br>
                      <p style="text-align:center"><img style="width:70%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/4. Peraturan/2. Mengunggah dokumen peraturan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Mengunduh atau menghapus dokumen peraturan</h3>
                    <div class="timeline-body">
                      Untuk mengunduh dokumen klik tombol berwarna biru, dan untuk menghapus dokumen klik tombol berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:20%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/4. Peraturan/3. Mengunduh atau menghapus dokumen peraturan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  H. Menu Penghargaan
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List Daftar Penghargaan</h3>
                    <div class="timeline-body">
                      Menu penghargaan digunakan untuk mengatur konten penghargaan yang ditampilkan pada halaman pengunjung. Pada halaman ini berisi gambar berupa penghargaan-penghargaan yang telah dicapai oleh Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kota Kediri.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/5. Penghargaan/1. List daftar menghargaan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Mengunggah data penghargaan</h3>
                    <div class="timeline-body">
                      Untuk Mengunggah file tekan tombol “tambah”, kemudian muncul form isian lalu isikan nama file dan pilih file yang ingin diunggah. Jika sudah tekan tombol “simpan”.
                      <br>
                      <p style="text-align:center"><img style="width:70%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/5. Penghargaan/2. Mengunggah data penghargaan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Menghapus data penghargaan</h3>
                    <div class="timeline-body">
                      untuk menghapus file klik tombol berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:25%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/5. Penghargaan/3. Menghapus data penghargaan.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  I. Menu Biodata Pejabat
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseSeven" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List daftar pejabat</h3>
                    <div class="timeline-body">
                      Menu Biodata pejabat digunakan untuk mengatur konten biodata pejabat yang ditampilkan pada halaman struktur organisasi yang dapat diakses oleh pengunjung. Pada halaman ini yang dapat dilakukan admin adalah (1) melihat list data pejabat, (2) melihat detail data pejabat dengan mengklik tombol action berwarna hijau, (3) menambah data pejabat dengan mengklik tombol “Tambah”, (4) mengedit data pejabat dengan mengklik tombol action berwa ungu, (5) dan menghapus data pejabat dengan mngklik tombol action berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/7. Biodata pejabat/1. List daftar pejabat.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Tambah data pejabat</h3>
                    <div class="timeline-body">
                      Untuk menambah data biodata pejabat harus mengisikan 4 field yang disediakan. (1) NIP : isikan nomor induk pegawai, (2) Nama : isikan nama pejabat, (3) Jabatan : Isikan jabatan pejabat, (4) photo : isikan foto pejabat ataupun gambar lain jika tidak ingin mengunggah foto pejabat yang bersangkutan.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/7. Biodata pejabat/2. Tambah data pejabat.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Edit data pejabat</h3>
                    <div class="timeline-body">
                      Form edit data pejabat untuk melakukan edit/update data NIP, Nama, Jabatan, ataupun foto pejabat.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/7. Biodata pejabat/3. Edit data pejabat.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  J. Menu Slideshow
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseEight" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List data slideshow</h3>
                    <div class="timeline-body">
                      Menu Slideshow digunakan untuk mengatur konten slideshow yang ditampilkan pada halaman “home” yang diakses oleh pengunjung. Admin hanya bisa menambahkan maksimal tiga gambar slideshow. Jika ingin mengganti gambar maka hapus dulu satu bambar kemudian tambahkan dengan satu gambar baru.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/8. Slideshow/1. List data slideshow.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Hapus gambar slideshow</h3>
                    <div class="timeline-body">
                      Untuk menghapus gambar slideshow klik tombol berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:25%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/8. Slideshow/2. Hapus gambar slideshow.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  K. Menu Pop-up
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseNine" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List data pop-up</h3>
                    <div class="timeline-body">
                      Menu pop-up digunakan untuk mengatur konten pop-up yang ditampilkan pada saat membuka halaman “home” yang diakses oleh pengunjung. Admin hanya bisa menambahkan maksimal tiga gambar pop-up. Jika ingin mengganti gambar maka hapus dulu satu bambar kemudian tambahkan dengan satu gambar baru.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/9. Popup/1. List data popup.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Hapus gambar pop-up</h3>
                    <div class="timeline-body">
                      Untuk menghapus gambar slideshow klik tombol berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:25%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/9. Popup/2. Hapus gambar popup.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </div>
          </div>
        </div>
        <!-- ============================================================================================ -->

        <!-- ============================================================================================== -->

        <div class="panel box box-success">
          <div class="box-header with-border">
            <h4 class="box-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" class="collapsed">
                <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                  L.	Menu Master User
                </span>
              </a>
            </h4>
          </div>
          <div id="collapseTen" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="box-body">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>1</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">List data user admin</h3>
                    <div class="timeline-body">
                      Menu ini digunakan untuk mengelola data user admin yang berhak mengakses aplikasi dengan hak akses admin. Pada menu ini yang dapat dilakukan admin adalah (1) menambah data user admin dengan mengklik tobol “Tambah”, (2) Mengedit data user admin dengan mengklik tombol action betrwarna ungu, dan (3) menghapus data user admin dengan mengklik tombol action berwarna merah.
                      <br>
                      <p style="text-align:center"><img style="width:90%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/10. Master User/1. List data user admin.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>2</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Tambah data user admin</h3>
                    <div class="timeline-body">
                      Untuk menambah data, admin admin harus mengisikan empat field yang disediakan yaitu (1) Nama : Isikan nama user admin, (2) Username : Isikan username yang digunakan  untuk login, (3) Password : isikan dengan password yang digunakan untuk login, serta (4) Confirm Password : masukkan ulang karakter yang sama pada field password. Kemudian klik tombol simpan untuk mengakhiri proses tambah data.
                      <br>
                      <p style="text-align:center"><img style="width:60%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/10. Master User/2. Tambah data user admin.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                  <i class="fa  bg-blue"><b>3</b></i>
                  <div class="timeline-item">
                    <h3 class="timeline-header">Edit data user admin</h3>
                    <div class="timeline-body">
                      Form edit data user admin diguakan untuk melakukan edit dan update data admin.
                      <br>
                      <p style="text-align:center"><img style="width:60%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/10. Master User/3. Edit data user admin.PNG')}}" alt="..." class="margin"></p>

                    </div>
                  </div>
                </li>
                <!-- END timeline item -->
              </ul>
            </ul>
          </div>
        </div>
      </div>
      <!-- ============================================================================================ -->

      <!-- ============================================================================================== -->

      <div class="panel box box-success">
        <div class="box-header with-border">
          <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" class="collapsed">
              <span style="padding-left:5px;padding-right:5px;" class="bg-green">
                M.	Menu Edit Profil dan Logout
              </span>
            </a>
          </h4>
        </div>
        <div id="collapseEleven" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
          <div class="box-body">
            <ul class="timeline">
              <!-- timeline time label -->
              <li class="time-label">
              </li>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                <i class="fa  bg-blue"><b>1</b></i>
                <div class="timeline-item">
                  <h3 class="timeline-header">Logout</h3>
                  <div class="timeline-body">
                    Klik menu logout untuk mengakhiri season.
                    <br>
                    <p style="text-align:center"><img style="width:20%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/11. Edit Profil dan logout/1. Logout.PNG')}}" alt="..." class="margin"></p>

                  </div>
                </div>
              </li>
              <!-- END timeline item -->
              <!-- timeline item -->
              <li>
                <i class="fa  bg-blue"><b>2</b></i>
                <div class="timeline-item">
                  <h3 class="timeline-header">Edit Profil</h3>
                  <div class="timeline-body">
                    Admin dapat mengubah nama, username, serta password.
                    <br>
                    <p style="text-align:center"><img style="width:60%;border:3px solid black;" src="{{asset('public/panduan_aplikasi/SS/11. Edit Profil dan logout/2. Edit profil.PNG')}}" alt="..." class="margin"></p>

                  </div>
                </div>
              </li>
              <!-- END timeline item -->
            </ul>
          </ul>
        </div>
      </div>
    </div>
    <!-- ============================================================================================ -->


  </div>
</div>
<!-- /.box-body -->
</div>












</section>
<!-- /.content -->
@endsection


@section('js')
@endsection
