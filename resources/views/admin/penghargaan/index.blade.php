<?php $hal = "admin_penghargaan"; ?>
@extends('layouts.admin.master')
@section('title', 'Penghargaan')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }

  .example-modal .modal {
    background: transparent !important;
  }

  /* .btn-delete-red{
    background-color:rgba(255, 0, 0, 0.5);
    color:#000;
    font-size:100%;
  }
  /* a.btn-delete-red:hover {
    background:#ffffff;
    background:-moz-linear-gradient(top,  #ffffff 0%, #f6f6f6 47%, #ededed 100%);
    background:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color- stop(47%,#f6f6f6), color-stop(100%,#ededed));
    background:-webkit-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:-o-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:-ms-linear-gradient(top,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    background:linear-gradient(to bottom,  #ffffff 0%,#f6f6f6 47%,#ededed 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 );color:#266CAE}
} */
</style>
@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Penghargaan
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Penghargaan</h3>
        </div>
        @if (count($errors) > 0)
          <ul style="color:red;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif
        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom:20px;margin-left:10px;" data-target="#modal-form"><i class="fa  fa-plus-square-o"></i>Tambah</button>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            @foreach ($penghargaan as $data)
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="box box-danger" style="background:#f7f8fa;">
                <div class="box-header with-border">
                  <h3 class="box-title">{{$data->penghargaan_judul}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <img src="{{asset('public/pengunjung/images/penghargaan/tumb/'.$data->penghargaan_gambar)}}" alt="">
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <form id="form1" onclick="return confirm('apakah anda yakin?')" action="{{route('admin_penghargaan.destroy',$data->penghargaan_id)}}" method="post">
                      {{csrf_field()}}
                      {{method_field("DELETE")}}
                    <button class="uppercase btn btn-danger"  type="submit" name="button">Hapus</button>
                  {{-- <a  href="{{route('admin_penghargaan.destroy',$data->penghargaan_id)}}" onclick="document.getElementById('form1').submit();" style="display:block;" class="uppercase"><b>Hapus</b></a> --}}
                </form>
                </div>
                <!-- /.box-footer -->
              </div>
            </div>
            @endforeach
            <!-- /.col -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@include('admin.penghargaan.form')

@endsection


@section('js')
<script type="text/javascript">
  function ConfirmDelete() {
    var x = confirm("Are you sure you want to delete?");
    if (x)
      return true;
    else
      return false;
  }
</script>


@endsection
