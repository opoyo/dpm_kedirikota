<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Default Modal</h4>
      </div>
      <form class="form-horizontal" action="{{route('admin_panduan.store')}}" data-toggle="validator" method="post" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('POST') }}
        <div class="modal-body">
          <input type="hidden" id="id" name="id">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
            <div class="input-group col-sm-8">
              <input type="text" class="form-control" required name="panduan_judul" id="panduan_judul" placeholder="Nama">
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <!-- /.form-group -->
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Dokumen</label>
            <div class="input-group col-sm-8">
              <input type="file" class="form-control" required name="panduan_file" id="panduan_file">
              <span class="help-block with-errors"></span>
            </div>
          </div>
          <!-- /.form-group -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning pull-left" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
          <button type="submit" id="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
        </div>
    </div>

    </form>

    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
