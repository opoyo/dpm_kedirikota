<?php $hal = "admin_biodata_pejabat"; ?>
@extends('layouts.admin.master')
@section('title', 'Biodata')

@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Biodata
		<!-- <small>Data barang</small> -->
	</h1>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">

			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Biodata Pejabata</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					@if(session()->has('msg'))
						<div class="alert alert-success">
							{{ session()->get('msg') }}
						</div>
						@endif
						<form role="form" action="{{route('master_biodata_pejabat.update',$biodata->biodata_pejabat_id)}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							{{method_field('PUT')}}
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1">NIP</label>
									<input type="text" class="form-control" value="{{$biodata->biodata_pejabat_nip}}" name="biodata_pejabat_nip" placeholder="NIP">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Nama</label>
									<input type="text" class="form-control" name="biodata_pejabat_nama" value="{{$biodata->biodata_pejabat_nama}}" placeholder="Nama Lengkap">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Jabatan</label>
									<input type="text" class="form-control" name="biodata_pejabat_jabatan" value="{{$biodata->biodata_pejabat_jabatan}}" placeholder="Jabatan">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Photo</label>
									<input type="file" class="form-control" name="biodata_pejabat_thumb" >
								</div>

								{{-- <div class="form-group">
               <label for="exampleInputFile">File input</label>
               <input type="file" id="exampleInputFile">

               <p class="help-block">Example block-level help text here.</p>
             </div> --}}
								{{-- <div class="checkbox">
               <label>
                 <input type="checkbox"> Check me out
               </label>
             </div> --}}
							</div>
							<!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->

@endsection

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$('.summernote').summernote({

			height: 300,

		});

	});
</script>

@endsection
