<?php $hal = "admin_biodata_pejabat"; ?>
@extends('layouts.admin.master')
@section('title', 'Biodata')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Biodata Pejabat
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Biodata Pejabat</h3>
        </div>
        <a href="{{route('master_biodata_pejabat.create')}}"  style="margin-bottom:20px;margin-left:10px;" class="card-body-title"><button class="btn btn-primary"><i class="fa  fa-plus-square-o"></i> Tambah</button></a>

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th style="width:2%;">No#</th>
              <th style="width:20%;">NIP</th>
              <th style="width:30%;">Nama</th>
              <th style="width:30%;">Jabatan</th>
              <th style="width:20%;">Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach ($biodata as $data)
            <tr>
              <td>{{$no++}}</td>
              {{-- <td>@php echo (strlen($data->berita_nama) > 30 ? substr($data->berita_nama,0,100)." ..." : $data->berita_nama) @endphp</td> --}}
              <td>{{$data->biodata_pejabat_nip}}</td>
              <td>{{$data->biodata_pejabat_nama}}</td>
              <td>{{$data->biodata_pejabat_jabatan}}</td>
              <td>
                <form  action="{{route('master_biodata_pejabat.destroy',$data->biodata_pejabat_id)}}" method="post">
                <a href="{{route('master_biodata_pejabat.show',$data->biodata_pejabat_id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="botttom" title="Detail Data"  style="color:white;"><i class="fa  fa-eye"></i></a>
                 <a href="{{route('master_biodata_pejabat.edit',$data->biodata_pejabat_id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Edit Data"  style="color:white;"><i class="fa  fa-edit"></i></a>
                   {{csrf_field()}}
                   {{method_field("DELETE")}}
                   <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"> </i></button>
                   {{-- <a  onclick='return confirm('Are you sure?')' class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"> </i></a> --}}
                 </form>
                 </td>
            </tr>
          @endforeach
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@endsection


@section('js')
<!-- DataTables -->
<script src="{{asset('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>



<script>
$(function () {
  $('#example1').DataTable()
  $('#example2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>



@endsection
