<?php $hal = "admin_biodata_pejabat"; ?>
@extends('layouts.admin.master')
@section('title', 'Biodata')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
    .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 30%;
}
  </style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Biodata
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Detail Biodata Pejabat</h3>
        </div>
        <a href="{{route('master_biodata_pejabat.index')}}" class="btn btn-primary" style=" margin-left: 20px;color:white;"> <i class="fa fa-arrow-left"></i> </a>
        <!-- /.box-header -->
        <div class="box-body">
          <img class="center" style="border-radius: 70%" src="{{asset('public/pengunjung/images/biodata/'.$biodata->biodata_pejabat_biodata_thumb)}}" alt="{{$biodata->biodata_pejabat_biodata_thumb}}">
          <h2 style="text-align:center;">{{$biodata->biodata_pejabat_nip}}</h2>

            <div class="row center" style="align:center; font-size:20px;">
              <div class="box">
              <div class="col-sm-3">
                <div>
                  <p>NIP</p>
                </div>
                <div>
                  <p>Nama</p>
                </div>
                <div>
                  <p>Jabatan</p>
                </div>
              </div>
              <div class="col-sm-1" style="margin-right:0;">
                <div>
                  <p>:</p>
                </div>
                <div>
                  <p>:</p>
                </div>
                <div>
                  <p>:</p>
                </div>
              </div>
              <div class="col-sm-5" style="margin-left:0;">
                <div>
                  <p>{{$biodata->biodata_pejabat_nip}}</p>
                </div>
                <div>
                  <p>{{$biodata->biodata_pejabat_nama}}</p>
                </div>
                <div>
                  <p>{{$biodata->biodata_pejabat_jabatan}}</p>
                </div>
              </div>
            </div>

            </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@endsection
