<?php $hal = "admin_pengaduan"; ?>
@extends('layouts.admin.master')
@section('title', 'Pengaduan')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }

  .example-modal .modal {
    background: transparent !important;
  }
</style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pengaduan
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Pengaduan</h3>
        </div>
        {{-- <a href="{{route('admin_berita.create')}}" style="margin-bottom:20px;margin-left:10px;" class="card-body-title"><button class="btn btn-primary"><i class="fa  fa-plus-square-o"></i> Tambah</button></a> --}}

        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width:2%;">No#</th>
                <th style="width:30%;">Nama</th>
                <th style="width:20%;">Status</th>
                <th style="width:20%;">Non-Aktif</th>
                <th style="width:28;">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pengaduan as $data)

              <tr id="{{$data->pengaduan_id}}">
                <td>{{$no++}}</td>
                <td>{{$data->pengaduan_nama}}</td>
                @if ($data->pengaduan_status_balas == 2)
                <td><span class="badge" style="background-color:#fca103">Belum Ada Tanggapan</span></td>
              @else
                <td> <span class="badge" style="background-color:green">Sudah di Tanggapi</span></td>

              @endif
                <td> <input type="checkbox" name="pengaduan_aktif" id="pengaduan_aktif"
                  @if ($data->pengaduan_aktif == '2')
                    checked
                  @endif
                  value="2"> </td>
                <td>
                  <form action="{{route('admin_pengaduan.destroy',$data->pengaduan_id)}}" method="post">
                    <a href="{{route('admin_pengaduan.show',$data->pengaduan_id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="botttom" title="Detail Data" style="color:white;"><i class="fa  fa-eye"></i></a>
                    @if ($data->pengaduan_status_balas == 2)
                    <button type="button" name="button" data-toggle="modal" data-id="{{$data->pengaduan_id}}" class="btn btn-primary open-form-balas"  data-placement="botttom" title="Balas Pengaduan" style="color:white;"><i class="fa  fa-edit"></i></button>
                    @else
                    <button type="button" disabled name="button" data-toggle="modal" data-id="{{$data->pengaduan_id}}" class="btn btn-primary open-form-balas"  data-placement="botttom" title="Balas Pengaduan" style="color:white;"><i class="fa  fa-edit"></i></button>
                    @endif
                    {{-- <a href="{{route('admin_berita.edit',$data->berita_id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="botttom" title="Balas Pengaduan" style="color:white;"><i class="fa  fa-edit"></i></a> --}}
                    {{csrf_field()}}
                    {{method_field("DELETE")}}
                    <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="botttom" title="Hapus Data" style="color:white;"><i class="fa  fa-trash"> </i></button>
                  </form>
                </td>
              </tr>
              @endforeach
              </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim balasan atas pengaduan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{url('admin_pengaduan/balas')}}" method="post">
          {{csrf_field()}} {{method_field("POST")}}
          <input type="hidden" name="id_pengaduan" id="id_pengaduan" value="">
          <label for="pengaduan_balas">Pesan</label>
          <textarea name="pengaduan_balas" class="form-control" rows="8" cols="80"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection


@section('js')
<!-- DataTables -->
<script src="{{asset('public/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>



<script>
// Open Modal With ID
$(document).on("click", ".open-form-balas", function () {
     var id_pengaduan = $(this).data('id');
     $(".modal-body #id_pengaduan").val( id_pengaduan );
    $('#modal-form').modal('show');
});

$(document).ready(function(){
  $("input:checkbox").change(function() {
    var pengaduan_id = $(this).closest('tr').attr('id');
    // console.log(program_id);
    $.ajax({
            type:'POST',
            url:'admin_pengaduan/update_aktif',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            data: { "id" : pengaduan_id },
            success: function(data){
              if(data.data.success){
                // console.log(data);
              }
            }
        });
    });
});

  $(function() {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging': true,
      'lengthChange': false,
      'searching': false,
      'ordering': true,
      'info': true,
      'autoWidth': false
    })
  })
</script>



@endsection
