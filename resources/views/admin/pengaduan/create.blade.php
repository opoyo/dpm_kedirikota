<?php $hal = "admin_berita"; ?>
@extends('layouts.admin.master')
@section('title', 'Berita')

@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Berita
		<!-- <small>Data barang</small> -->
	</h1>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">

			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Berita</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					@if(session()->has('msg'))
						<div class="alert alert-success">
							{{ session()->get('msg') }}
						</div>
						@endif
						<form role="form" action="{{route('admin_berita.store')}}" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							{{method_field('post')}}
							<div class="box-body">
								<div class="form-group">
									<label for="exampleInputEmail1">Judul</label>
									<input type="text" class="form-control" name="berita_nama" placeholder="Judul Berita">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Isi</label>
									<textarea class="form-control summernote" name="berita_isi"></textarea>
								</div>
								{{-- <div class="form-group">
               <label for="exampleInputFile">File input</label>
               <input type="file" id="exampleInputFile">

               <p class="help-block">Example block-level help text here.</p>
             </div> --}}
								{{-- <div class="checkbox">
               <label>
                 <input type="checkbox"> Check me out
               </label>
             </div> --}}
							</div>
							<!-- /.box-body -->

							<div class="box-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->

@endsection

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$('.summernote').summernote({

			height: 300,

		});

	});
</script>

@endsection
