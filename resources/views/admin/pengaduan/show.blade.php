<?php $hal = "admin_pengaduan"; ?>
@extends('layouts.admin.master')
@section('title', 'Berita')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
    .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 30%;
}
  </style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pengaduan
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Detail </h3>
        </div>
        <a href="{{route('admin_pengaduan.index')}}" class="btn btn-primary" style=" margin-left: 20px;color:white;"> <i class="fa fa-arrow-left"></i> </a>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-lg-5">
              <div class="box">
                <h2>Profile</h2>
                <div class="col-md-12">
                  <div class="col-md-6">
                      <label for="">Nama</label>
                  </div>
                  <div class="col-md-6">
                      <p>{{$pengaduan->pengaduan_nama}}</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                      <label for="">Email</label>
                  </div>
                  <div class="col-md-6">
                      <p>{{$pengaduan->pengaduan_email}}</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                      <label for="">No Telp</label>
                  </div>
                  <div class="col-md-6">
                      <p>{{$pengaduan->pengaduan_telp}}</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                      <label for="">Alamat</label>
                  </div>
                  <div class="col-md-6">
                      <p>{{$pengaduan->pengaduan_alamat}}</p>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-lg-7">
              <div class="box">
                <h2>Pengaduan</h2>
                <div class="col-md-12">
                  <p>{{$pengaduan->pengaduan_data}}</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@endsection
