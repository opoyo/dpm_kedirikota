<?php $hal = "admin_peraturan"; ?>
@extends('layouts.admin.master')
@section('title', 'Peraturan')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }

  .example-modal .modal {
    background: transparent !important;
  }
</style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Peraturan
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Dokumen Peraturan</h3>
        </div>
        @if (count($errors) > 0)
          <ul style="color:red;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif
        <button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom:20px;margin-left:10px;" data-target="#modal-form"><i class="fa  fa-plus-square-o"></i>Tambah</button>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            @foreach ($peraturan as $data)
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box box-success" style="background:#f7f8fa;">
                <a target="_blank" href="{{asset('public/files/per/'.$data->peraturan_file)}}" style="color:white;"><span class="info-box-icon bg-blue"> <i class="glyphicon glyphicon-arrow-down"></i></span></a>
                <div class="info-box-content">
                  <p style="color:#000">
                    @php echo (strlen($data->peraturan_judul) > 40 ? substr($data->peraturan_judul,0,40)." ..." : $data->peraturan_judul)
                    @endphp</p>
                    <form  action="{{route('admin_peraturan.destroy',$data->peraturan_id)}}" method="post">
                      {{ csrf_field() }}
                      {{method_field("DELETE")}}
                      <button onclick="return confirm('Are you sure?')" style="position:relative;" type="submit" class="btn btn-danger" name="button">Hapus</button>
                    </form>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            @endforeach
            <!-- /.col -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@include('admin.peraturan.form')

@endsection


@section('js')



@endsection
