<?php $hal = "admin_potensi"; ?>
@extends('layouts.admin.master')
@section('title', 'Potensi')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/bower_components/select2/dist/css/select2.min.css')}}">

<style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
    .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 30%;
}
  </style>

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Potensi
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Detail Data Potensi</h3>
        </div>
        <a href="{{route('admin_potensi.index')}}" class="btn btn-primary" style=" margin-left: 20px;color:white;"> <i class="fa fa-arrow-left"></i> </a>
        <!-- /.box-header -->
        <div class="box-body">
          <h2 style="text-align:center;">{{$potensi->potensi_nama}}</h2>
          <img class="center" src="{{asset('public/pengunjung/images/potensi/'.$potensi->potensi_gambar)}}" alt="{{$potensi->potensi_gambar}}">

          {!! $potensi->potensi_isi !!}
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@endsection
