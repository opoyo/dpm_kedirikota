<?php $hal = "admin_potensi"; ?>
@extends('layouts.admin.master')
@section('title', 'Potensi')

@section('css')
<link rel="stylesheet" href="{{ asset('public/admin/dist/css/summernote.css') }}">

@endsection


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Potensi
    <!-- <small>Data barang</small> -->
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Potensi</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          @if(session()->has('msg'))
          <div class="alert alert-success">
            {{ session()->get('msg') }}
          </div>
          @endif
          <form role="form" action="{{route('admin_potensi.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('post')}}
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Potensi</label>
                <input type="text" class="form-control" name="potensi_nama" placeholder="Judul Potensi">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Isi</label>
                <textarea class="form-control summernote" name="potensi_isi"></textarea>
              </div>
              {{-- <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input type="file" id="exampleInputFile">

                <p class="help-block">Example block-level help text here.</p>
              </div> --}}
              {{-- <div class="checkbox">
                <label>
                  <input type="checkbox"> Check me out
                </label>
              </div> --}}
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@endsection

@section('js')
<script src="{{ asset('public/admin/dist/js/summernote.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {

  $('.summernote').summernote({

    height: 300,

  });

});
</script>

@endsection
