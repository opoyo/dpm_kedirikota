<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<div class="slider-container rev_slider_wrapper" style="height: 100%;">
		<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.7">
			<ul>

				@foreach($slideshow as $slideshow_data)

				<li data-transition="fade">
					<img src="{{ asset('public/pengunjung/images/slide') }}/{{$slideshow_data->slideshow_gambar}}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg">
				</li>

				@endforeach


			</ul>
		</div>
	</div>
	<section class="looking-for custom-position-1 mb-4">
		<div class="container custom-md-border-top">
			<div class="row align-items-center">
				<div class="col-md-7 col-lg-7">
					<div class="looking-for-box">
						<!-- <h2 class="text-6">- <span class="text-2 custom-secondary-font">DPMPTSP</span><br>
						PEMERINTAH KOTA KEDIRI</h2>
						<span class="white-text">Jl. Basuki Rahmad No.15 Kota Kediri</span> -->
					</div>
				</div>
				<div class="col-md-5 d-flex justify-content-md-end mb-4 mb-md-0">
					<!-- <a class="text-decoration-none" href="#" target="_blank" title="Call Us Now">
					<span class="custom-call-to-action">
					<span class="action-title text-color-light">Hubungi Kami</span>
					<span class="action-info text-color-light">(0354) 682345</span>
					<span class="action-info text-color-light">bpm.kotakediri@gmail.com</span>
				</span>
			</a> -->
		</div>
	</div>
</div>
</section>
<br class="clearfix" />
<div class="container mb-4" style="margin-top: 100px;">
	<div class="row">
		<div class="col-md-12 center">
			<div class="heading heading-border heading-middle-border heading-middle-border-center">
				<h1><strong>LAYANAN</strong> KAMI</h1>
			</div>
		</div>
	</div>
	<div class="row mt-md mb-x1">
		<div class="col-md-4">
			<div class="thumb-info custom-thumb-info-4">
				<!-- <div class="thumb-info-wrapper"><img src="http://[::1]/portal_kedirikota/assets/images/perizinan-online.jpg" class="img-responsive"></div> -->
				<div class="thumb-info-wrapper"><img src="{{ asset('public/pengunjung/images/ICON_OSS.png') }} " class="img-responsive"></div>
				<div class="thumb-info-caption custom-box-shadow">
					<div class="thumb-info-caption-text">
						<!-- <h4 class="text-center"><a href="http://[::1]/portal_kedirikota/site/layanan/izin" class="text-color-dark"> OSS Republik Indonesia</a></h4> -->
						<h4 class="text-center"><a  target="_blank" href="https://www.oss.go.id/oss/" class="text-color-dark"> OSS Republik Indonesia</a></h4>
						<p class="justify" style="min-height:100px;">Kemudahan berusaha dalam berbagai skala turut didorong Pemerintah dengan reformasi struktural,</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="thumb-info custom-thumb-info-4">
				<div class="thumb-info-wrapper"><img src="{{ asset('public/pengunjung/images/ICON_KSWI.png') }}  " class="img-responsive"></div>
				<div class="thumb-info-caption custom-box-shadow">
					<div class="thumb-info-caption-text">
						<h4 class="text-center"><a target="_blank" href="https://kswi.kedirikota.go.id/" class="text-color-dark"> KSWI</a></h4>
						<p class="justify" style="min-height:100px;">KEDIRI SINGLE WINDOW FOR INVESTMENT. Layanan Perizinan Online. Laporan Perizinan. Persyaratan Perizinan.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-3">
			<div class="thumb-info custom-thumb-info-4">
				<div class="thumb-info-wrapper"><img src=" {{ asset('public/pengunjung/images/ICON_SIMPEL.png') }} " class="img-responsive"></div>
				<div class="thumb-info-caption custom-box-shadow">
					<div class="thumb-info-caption-text">
						<h4 class="text-center"><a target="_blank" href="https://lkpmonline.bkpm.go.id/lkpm_perka17/login.jsp" class="text-color-dark"> LKPM Online</a></h4>
						<p class="justify" style="min-height:100px;">LKPM dilengkapi dengan fitur self assesment disertai informasi pada tiap isian untuk memudahkan anda dalam melaporkan LKPM</p>
					</div>
				</div>
			</div>
		</div> -->
		<div class="col-md-4">
			<div class="thumb-info custom-thumb-info-4">
				<div class="thumb-info-wrapper"><img src=" {{ asset('public/pengunjung/images/ICON_MPS.png') }} " class="img-responsive"></div>
				<div class="thumb-info-caption custom-box-shadow">
					<div class="thumb-info-caption-text">
						<h4 class="text-center"><a href="{{url('site_inovasi_layanan')}}" class="text-color-dark"> INOVASI LAYANAN</a></h4>
						<p class="justify" style="min-height:100px;">Dapatkan kemudahan dalam proses pelayanan perizinan melalui berbagai inovasi layanan</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container mb-4" >
	<div class="row">
		<div class="col-md-12 center">
			<div class="heading heading-border heading-middle-border heading-middle-border-center">
				<h1><strong>VIDEO</strong> MOTION</h1>
			</div>
		</div>
	</div>
	<div class="row mt-md mb-xl">

		@foreach($video as $data_video)
		<div class="col-md-6" style="margin-bottom:10px;">
			<div><div class="team-item p-0">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="{{$data_video->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div></div>
		</div>

		@endforeach

		<h5 class="font-weight-bold" style="margin-top:10px;margin-left:15px;"><a target="_blank" href="https://www.youtube.com/channel/UCclzH8UOi24s0duCywBcuGw">lihat video lebih lanjut di channel Youtube kami ...</a></h5>
	</div>
</div>


<!--
<div class="clearfix"></div>
<div class="container mb-4">
<div class="row mt-md mb-xl">
<div class="col-md-4 white-text pt-5 m-0" style="background-color: #755da9;">
<div class="hidden-xs hidden-sm" style="margin-top:50px;"></div>
<div class="">
<span class="text-7" style="line-height:2rem; font-size: 1rem !important;">TRACKING LAYANAN K.S.W.I :</span>
<form class="custom-contact-form-style-1" style="margin-top: 7px;">
<div class="form-row">
<div class="form-group col">
<div class="custom-input-box">
<i class="fas fa-list-alt icons text-color-primary"></i>
<input type="text" value="" data-msg-required="masukkan nomer pendaftaran" maxlength="100" class="form-control" name="name" id="name" placeholder="masukkan nomer resi" required="" style="border-radius: 0 0px 0px 0 !important;">
</div>
</div>
</div>
<div class="form-row">
<div class="form-group col">
<input type="submit" value="search" class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" data-loading-text="Loading..." style="background: #00FF00; border-color: #00FF00; color: #000;">
</div>
</div>
</form>
</div>
</div>
<div class="col-md-8 p-0 m-0">
<div class="owl-carousel show-nav-title custom-dots-style-1 custom-dots-position custom-xs-arrows-style-2 mb-0"
data-plugin-options="{'autoplay':true,'autoplayTimeout':5000,'items': 1, 'margin': 20, 'autoHeight': true, 'loop': true, 'nav': false, 'dots': false}">
<div><div class="team-item p-0">
<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eb6W_CNQ3zg?modestbranding=0&showinfo=0&rel=0"></iframe>
</div>
</div></div>
</div>

</div>
</div>
</div>
<div class="clearfix"></div> -->




<div class="container mb-4">
	<div class="row">
		<div class="col-md-12 center">
			<div class="heading heading-border heading-middle-border heading-middle-border-center">
				<h1><strong>PUBLIKASI</strong> KAMI</h1>
			</div>
		</div>
	</div>
	<div class="row mt-md mb-xl">
		<!-- <div class="col-md-1"></div> -->
		<div class="blog-post col-md-12">

			@foreach($berita as $data)

			<article class="blog-post col">
				<div class="row">
					<div class="col-sm-12 col-lg-4">
						<div class="blog-post-image-wrapper">
							<a href="{{url('site_berita')}}/{{$data->berita_id}}"><img src="{{ asset('public/pengunjung/images/berita/') }}/{{$data->berita_gambar}} " alt="" class="img-fluid mb-4"></a>
							<span class="blog-post-date background-color-primary text-color-light font-weight-bold">

								<?php

								$tanggal = substr($data->berita_tanggal, 8, 2);
								$bulan = substr($data->berita_tanggal, 5, 2);
								$tahun = substr($data->berita_tanggal, 2, 2);

								$bulan_text = "";

								if ($bulan == "01") {
									$bulan_text = "Jan";
								}elseif ($bulan == "02") {
									$bulan_text = "Feb";
								}elseif ($bulan == "03") {
									$bulan_text = "Mar";
								}elseif ($bulan == "04") {
									$bulan_text = "Apr";
								}elseif ($bulan == "05") {
									$bulan_text = "May";
								}elseif ($bulan == "06") {
									$bulan_text = "Jun";
								}elseif ($bulan == "07") {
									$bulan_text = "Jul";
								}elseif ($bulan == "08") {
									$bulan_text = "Aug";
								}elseif ($bulan == "09") {
									$bulan_text = "Sep";
								}elseif ($bulan == "10") {
									$bulan_text = "Oct";
								}elseif ($bulan == "11") {
									$bulan_text = "Nov";
								}elseif ($bulan == "12") {
									$bulan_text = "Dec";
								}

								?>

								{{$tanggal}}<span class="month-year font-weight-light">{{$bulan_text}}-{{$tahun}}</span>
							</span>
						</div>
					</div>
					<div class="col-sm-12 col-lg-8">
						<h4 class="font-weight-bold"><a href="{{url('site_berita')}}/{{$data->berita_id}}">{{$data->berita_nama}}</a></h4>
						<hr class="solid mt-1 mb-1">
						<p style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
							<?php $isi =  substr(strip_tags($data->berita_isi),0,400); ?>
							{!!$isi!!} . . .
						</p>
					</div>
				</div>
			</article>

			@endforeach


			<br class="clearfix" />
			<div class="row text-center mt-1">
				<div class="col">
					<a class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" href="{{url('site_berita')}}">Tampilkan Semua Publikasi</a>
				</div>
			</div>
		</div>
		<!-- <div class="elfsight-app-87c5e3c4-4a01-4269-91fd-330f6078f8ad"></div> -->
	</div>
</div>
</div>
<section class="section-secondary custom-section-padding">
	<div class="container">
		<div class="row">
			<div class="col">
				<h2 class="font-weight-bold" style="color: #fff;"><center>Link Terkait</center></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-12" style="max-width: 51%;">
				<div class="owl-carousel show-nav-title custom-dots-style-1 custom-dots-position custom-xs-arrows-style-2 mb-0"
				data-plugin-options="{'autoplay':true,'autoplayTimeout':3000,'items': 4, 'margin': 20, 'autoHeight': true, 'loop': true, 'nav': false, 'dots': false}">
				<div><div class="team-item p-0">
					<a target="_blank" href="https://www.bkpm.go.id/" class="text-decoration-none">
						<span class="image-wrapper"><img src="{{ asset('public/pengunjung/images/banner/ICON_BKPM.png') }}" alt="" class="img-fluid"/></span></a>
					</div></div>
					<div><div class="team-item p-0">
						<a target="_blank" href="https://kedirikota.go.id/" class="text-decoration-none">
							<span class="image-wrapper"><img src="{{ asset('public/pengunjung/images/banner/ICON_PEMKOT.png') }}" alt="" class="img-fluid" /></span></a>
						</div></div>
						<div><div class="team-item p-0">
							<a target="_blank" href="http://dispendukcapil.kedirikota.go.id/" class="text-decoration-none">
								<span class="image-wrapper"><img src="{{ asset('public/pengunjung/images/banner/ICON_DISPENDUK.png') }}" alt="" class="img-fluid" /></span></a>
							</div></div>
							<div><div class="team-item p-0">
								<a target="_blank" href="https://diskominfo.kedirikota.go.id/" class="text-decoration-none">
									<span class="image-wrapper"><img src="{{ asset('public/pengunjung/images/banner/ICON_KOMINFO.png') }}" alt="" class="img-fluid" /></span></a>
								</div></div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	@if($popup_check->popup_check_value==1)
		<div class="modal fade in" id="global-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block; padding-right: 17px;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<!-- <div id="popup" class="carousel slide" data-ride="carousel">
						<button type="button" class="btn btn-default tutup" data-dismiss="modal" aria-hidden="true" style="position: absolute; z-index: 15; text-align: center; list-style: none; margin-left: 93%; text-shadow: 0 1px 2px rgba(0, 0, 0, .6); opacity: .5;">
						<span class="glyphicon glyphicon-remove">X</span>
					</button>
					<ol class="carousel-indicators">
					<li data-target="#popup" data-slide-to="0" class="active"></li>
				</ol>
				<div class="carousel-inner">
				<div class="item">
				<img src="http://[::1]/portal_kedirikota/assets/images/popup/popup25.jpg" class="img-responsive" height="600px">
			</div>
		</div>
	</div> -->
	<!-- </div> -->
	<!-- </div> -->

	<div id="popup" class="carousel slide" data-ride="carousel">
		<button type="button" class="btn btn-default tutup" data-dismiss="modal" aria-hidden="true" style="position: absolute; z-index: 15; text-align: center; list-style: none; margin-left: 93%; text-shadow: 0 1px 2px rgba(0, 0, 0, .6); opacity: .5;">
			<span class="glyphicon glyphicon-remove">X</span>
		</button>
		<div class="owl-carousel owl-theme nav-bottom rounded-nav numbered-dots pl-1 pr-1" data-plugin-options="{'items': 1, 'loop': true, 'dots': true, 'nav': false}" style="margin-bottom: -2px;">
			@foreach($popup as $pop)
			<div class="item">
				<img src="{{ asset('public/pengunjung/images/popup') }}/{{$pop->popup_gambar}}" class="img-responsive" height="600px">
			</div>
			@endforeach
		</div>
	</div>



</div>
</div>
</div>
</div>
@endif
@endsection


@section('js')
<script src="https://apps.elfsight.com/p/platform.js" defer></script>

@endsection
