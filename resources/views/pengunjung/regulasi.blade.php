<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>REGULASI <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active">Regulasi</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">
				<ol style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: #777777; font-family: 'Open Sans', Arial, sans-serif;">
<li style="box-sizing: border-box; line-height: 24px;">Undang-Undang Nomor 25 Tahun 2009 Tentang Pelayanan Publik</li>
<li style="box-sizing: border-box; line-height: 24px;">Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah</li>
<li style="box-sizing: border-box; line-height: 24px;">Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintah Daerah</li>
<li style="box-sizing: border-box; line-height: 24px;">Peraturan Presiden Nomor 27 Tahun 2009 tentang Pelayanan Terpadu Satu Pintu di Bidang Penanaman Modal</li>
<li style="box-sizing: border-box; line-height: 24px;">Peraturan Presiden Nomor 97 Tahun 2014 tentang Penyelenggaraan Pelayanan Terpadu Satu Pintu</li>
<li style="box-sizing: border-box; line-height: 24px;">Perda yang di cabut dan/atau direvisi oleh Kementrian Dalam Negeri</li>
<li style="box-sizing: border-box; line-height: 24px;">Perpres Nomor 44 Tahun 2016</li>
</ol>				<!-- <ol>
					<li>Undang-Undang Nomor 25 Tahun 2009 Tentang Pelayanan Publik</li>
					<li>Undang-Undang Nomor 28 Tahun 2009 tentang Pajak Daerah dan Retribusi Daerah</li>
					<li>Undang-Undang Nomor 23 Tahun 2014 tentang Pemerintah Daerah</li>
					<li>Peraturan Presiden Nomor 27 Tahun 2009 tentang Pelayanan Terpadu Satu Pintu di Bidang Penanaman Modal</li>
					<li>Peraturan Presiden Nomor 97 Tahun 2014 tentang Penyelenggaraan Pelayanan Terpadu Satu Pintu</li>
					<li>Perda yang di cabut dan/atau direvisi oleh Kementrian Dalam Negeri</li>
					<li>Perpres Nomor 44 Tahun 2016</li>
				</ol> -->
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')
@endsection
