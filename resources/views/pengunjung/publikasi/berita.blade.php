<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')

<style type="text/css">
.responsive {
  width: 100%;
  height: auto;
}
.pagination {
  display: inline-block;
}

.pagination li {
  color: white;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
	border: 1px solid #ddd; /* Gray */

}

.pagination li:first-child {
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.pagination li:last-child {
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.pagination li.active {
  background-color: #964dbb;
  color: white;
  border: 1px solid #964dbb ;
}

.pagination li.active span {
  background-color: #ffffff;
  color: #ffffff;
}

.pagination li:hover:not(.active) {background-color: #ddd;}



</style>

@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>BERITA <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li>Publikasi</li>
						<li class="active">Berita</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">


			<div class="col-lg-8 pt-1 pb-4 mb-3">

@foreach($berita as $data)

<article class="blog-post col">
	<div class="row">
		<div class="col-sm-12 col-lg-4">
			<div class="blog-post-image-wrapper">
				<a href="{{url('site_berita')}}/{{$data->berita_id}}"><img src="{{ asset('public/pengunjung/images/berita/') }}/{{$data->berita_gambar}} " alt="" class="img-fluid mb-4"></a>
				<span class="blog-post-date background-color-primary text-color-light font-weight-bold">

					@php

					$tanggal = substr($data->berita_tanggal, 8, 2);
					$bulan = substr($data->berita_tanggal, 5, 2);
					$tahun = substr($data->berita_tanggal, 2, 2);

					$bulan_text = "";

					if ($bulan == "01") {
						$bulan_text = "Jan";
					}elseif ($bulan == "02") {
						$bulan_text = "Feb";
					}elseif ($bulan == "03") {
						$bulan_text = "Mar";
					}elseif ($bulan == "04") {
						$bulan_text = "Apr";
					}elseif ($bulan == "05") {
						$bulan_text = "May";
					}elseif ($bulan == "06") {
						$bulan_text = "Jun";
					}elseif ($bulan == "07") {
						$bulan_text = "Jul";
					}elseif ($bulan == "08") {
						$bulan_text = "Aug";
					}elseif ($bulan == "09") {
						$bulan_text = "Sep";
					}elseif ($bulan == "10") {
						$bulan_text = "Oct";
					}elseif ($bulan == "11") {
						$bulan_text = "Nov";
					}elseif ($bulan == "12") {
						$bulan_text = "Dec";
					}

					@endphp

					{{$tanggal}}<span class="month-year font-weight-light">{{$bulan_text}}-{{$tahun}}</span>
				</span>
			</div>
		</div>
		<div class="col-sm-12 col-lg-8">
			<h4 class="font-weight-bold"><a href="{{url('site_berita')}}/{{$data->berita_id}}">{{$data->berita_nama}}</a></h4>
			<hr class="solid mt-1 mb-1">
			<p style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
				@php $isi =  substr(strip_tags($data->berita_isi),0,400); @endphp
				{{$isi}} . . .
			</p>
		</div>
	</div>
</article>

@endforeach
<br>
<div class="" style="text-align: center;">
{{$berita->links()}}
</div>




</div>
			<div class="col-lg-4 pt-1 pb-4 mb-3">

        <div class="elfsight-app-87c5e3c4-4a01-4269-91fd-330f6078f8ad"></div>


</div>
</div>
</div>
</div>


@endsection


@section('js')
  <script src="https://apps.elfsight.com/p/platform.js" defer></script>

<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
  }
</script>
@endsection
