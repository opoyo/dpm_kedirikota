<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')

<style type="text/css">
.responsive {
  width: 100%;
  height: auto;
}

.pagination {
  display: inline-block;
}

.pagination li {
  color: white;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  border: 1px solid #ddd; /* Gray */

}

.pagination li:first-child {
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.pagination li:last-child {
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.pagination li.active {
  background-color: #964dbb;
  color: white;
  border: 1px solid #964dbb ;
}

.pagination li.active span {
  background-color: #ffffff;
  color: #ffffff;
}

.pagination li:hover:not(.active) {background-color: #ddd;}



</style>

@endsection


@section('content')

<div role="main" class="main">
  <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          @foreach($berita_detail as $nama)
          <h1>{{$nama->berita_nama}} <span>Berita</span></h1>
          @endforeach
        </div>
        <div class="col-lg-6">
          <ul class="breadcrumb pull-right">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="#">Berita</a></li>
            <li class="active">Detail</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 pt-1 pb-4 mb-3" style="text-align:justify;">

        @foreach($berita_detail as $detail)
        <div class="post-infos mb-4">
          <span class="info posted-by">
            Ditulis oleh:
            <span class="post-author font-weight-semibold text-color-dark">
              {{$detail->name}}
            </span>
          </span>
          <span class="info like ml-4">
            Tanggal Terbit:
            <span class="like-number font-weight-semibold custom-color-dark">

              <?php

              $tanggal = substr($detail->berita_tanggal, 8, 2);
              $bulan = substr($detail->berita_tanggal, 5, 2);
              $tahun = substr($detail->berita_tanggal, 2, 2);

              $bulan_text = "";

              if ($bulan == "01") {
                $bulan_text = "Jan";
              }elseif ($bulan == "02") {
                $bulan_text = "Feb";
              }elseif ($bulan == "03") {
                $bulan_text = "Mar";
              }elseif ($bulan == "04") {
                $bulan_text = "Apr";
              }elseif ($bulan == "05") {
                $bulan_text = "May";
              }elseif ($bulan == "06") {
                $bulan_text = "Jun";
              }elseif ($bulan == "07") {
                $bulan_text = "Jul";
              }elseif ($bulan == "08") {
                $bulan_text = "Aug";
              }elseif ($bulan == "09") {
                $bulan_text = "Sep";
              }elseif ($bulan == "10") {
                $bulan_text = "Oct";
              }elseif ($bulan == "11") {
                $bulan_text = "Nov";
              }elseif ($bulan == "12") {
                $bulan_text = "Dec";
              }

              ?>

              {{$tanggal}}<span class="month-year font-weight-light">{{$bulan_text}}, {{$tahun}}</span>
            </span>
          </span>
        </div>
        <p><img src="{{asset('public/pengunjung/images/berita')}}/{{$detail->berita_gambar}}" class="responsive" /></p>

        {!!$detail->berita_isi!!}

        @endforeach

      </div>
       <div class="col-lg-4 pt-1 pb-4 mb-3 center">
        <div class="heading heading-border heading-middle-border heading-middle-border-center">
          <h2><strong>Berita Lain</strong></h2>
        </div>
        <div class="owl-carousel owl-theme nav-bottom rounded-nav numbered-dots pl-1 pr-1" data-plugin-options="{'items': 1, 'loop': true, 'dots': true, 'nav': false}">

          @foreach($berita as $data)
          <div>
            <div class="custom-step-item">
              <span class="step text-uppercase">

                @php

                $tanggal = substr($data->berita_tanggal, 8, 2);
                $bulan = substr($data->berita_tanggal, 5, 2);
                $tahun = substr($data->berita_tanggal, 2, 2);

                $bulan_text = "";

                if ($bulan == "01") {
                  $bulan_text = "Jan";
                }elseif ($bulan == "02") {
                  $bulan_text = "Feb";
                }elseif ($bulan == "03") {
                  $bulan_text = "Mar";
                }elseif ($bulan == "04") {
                  $bulan_text = "Apr";
                }elseif ($bulan == "05") {
                  $bulan_text = "May";
                }elseif ($bulan == "06") {
                  $bulan_text = "Jun";
                }elseif ($bulan == "07") {
                  $bulan_text = "Jul";
                }elseif ($bulan == "08") {
                  $bulan_text = "Aug";
                }elseif ($bulan == "09") {
                  $bulan_text = "Sep";
                }elseif ($bulan == "10") {
                  $bulan_text = "Oct";
                }elseif ($bulan == "11") {
                  $bulan_text = "Nov";
                }elseif ($bulan == "12") {
                  $bulan_text = "Dec";
                }

                @endphp

                {{$bulan_text}} '{{$tahun}}
                <span class="step-number text-color-primary">
                  {{$tanggal}}
                </span>

              </span>
              <div class="step-content">
                <h4 class="mb-4"><a href="{{url('site_berita')}}/{{$data->berita_id}}"><strong>{{$data->berita_nama}}</strong></a></h4>

                @php $isi =  substr($data->berita_isi, 0, 300); @endphp
                {!!$isi!!} . . .

              </div>
            </div>
          </div>

          @endforeach

        </div>
      </div> 
    </div>
  </div>
</div>



@endsection


@section('js')
@endsection
