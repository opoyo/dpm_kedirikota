<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>PENGHARGAAN <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li ><a href="#">Tentang</a></li>
						<li class="active">Penghargaan</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">
				<div class="row">

					@foreach($penghargaan as $data)
					<div class="col-md-6 col-xs-12">
						<center>

							<b style="font-size:20px;">{{$data->penghargaan_judul}}</b><br><br>
							<div class="" style="border:1px solid; border-radius: 12px;z-index:1;position:relative;overflow:hidden;width:75%">
								<div style="width:100%;">
									<img src="{{asset('public/pengunjung/images/penghargaan')}}/{{$data->penghargaan_gambar}}" style="width:100%;z-index:-1" class="img-responsive">
								</div>
							</div>
						</center>
						<br class="clear-fix">
					</div>

					@endforeach

				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')
@endsection
