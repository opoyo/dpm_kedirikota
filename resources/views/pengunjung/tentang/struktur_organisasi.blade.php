<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>STRUKTUR ORGANISASI <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li><a href="#">Tentang</a></li>
						<li class="active">Struktur Organisasi</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">

				@foreach($struktur_organisasi as $data)

				<div class="contact-form-wrap" style="margin-top: 20px;margin-bottom: 20px">
					<div align="center" data-animate="fadeInUp" data-delay=".7" style="animation-duration: 0.6s; animation-delay: 0.7s;" class="animated fadeInUp">
						<img align="center" src="{{asset('public/pengunjung/images/struktur_organisasi')}}/{{$data->struktur_organisasi_gambar}}" width="100%" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;" class="animated fadeInUp">
					</div>
				</div>

				@endforeach


				<div class="container mb-4">
					<div class="row">
						<div class="col-md-12 center">
							<div class="heading heading-border heading-middle-border heading-middle-border-center">
								<h2><strong>BIODATA</strong> PEJABAT</h2>
							</div>
						</div>
					</div>

					@foreach($biodata as $bio)
					<div class="row mb-4 pb-4">
						<div class="col-8 col-sm-4 col-lg-2 text-center">
							<img width="60%" src="{{ asset('public/pengunjung/images/biodata') }}/{{$bio->biodata_pejabat_biodata_thumb}}" alt="" class="img-fluid custom-rounded-image shadow hoverable">
						</div>
						<div class="col-12 col-sm-12 col-lg-10">
							<h3 class="mb-0">{{$bio->biodata_pejabat_nama}}</h3>
							<div class="testimonial custom-testimonial-style-1 testimonial-with-quotes mb-0">
								<div class="testimonial-author float-left">
									<p><strong>NIP: {{$bio->biodata_pejabat_nip}}</strong><span class="text-color-primary font-weight-bold">{{$bio->biodata_pejabat_jabatan}}</span></p>
								</div>
							</div>
						</div>
					</div>
					@endforeach


				</div>



			</div>
		</div>
	</div>
</div>



@endsection


@section('js')
@endsection
