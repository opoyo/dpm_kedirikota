<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
<style type="text/css" >

</style>
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header well well-lg page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>GALERI <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active">Galeri</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
						<!-- <div class="elfsight-app-87c5e3c4-4a01-4269-91fd-330f6078f8ad"></div> -->

			@foreach($galeri as $data)

			<div class="col-md-4 " style="margin-bottom:20px;">
				<div class="card" style="width: 100%;">
					<a href="{{url('site_galeri')}}/{{$data->galeri_id}}">
						<img class="card-img-top" src="{{asset('public/pengunjung/images/galeri/'.$data->galeri_gambar)}}" alt="Card image cap">
					</a>
					<div class="card-body">
						<p class="card-text"> <a href="#"> {{$data->galeri_nama}}</a></p>
					</div>
				</div>
			</div>

			@endforeach

		</div>
	</div>
</div>

@endsection


@section('js')
<!-- <script src="https://apps.elfsight.com/p/platform.js" defer></script> -->
@endsection
