<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/ekko-lightbox.css') }}">

<style type="text/css" >
.row {
	margin: 15px;
}
</style>
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header well well-lg page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1 style="font-size:25px !important;">{{$galeri->galeri_nama}} <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="#">Galeri</li>
						<li class="active">Detail</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">

			<a style="margin-bottom:20px;" href="{{asset('public/pengunjung/images/galeri/'.$galeri->galeri_gambar)}}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
				<img src="{{asset('public/pengunjung/images/galeri/'.$galeri->galeri_gambar)}}" class="img-fluid rounded">
			</a>

			@foreach($galeri_konten as $data)
			<a style="margin-bottom:20px;" href="{{asset('public/pengunjung/images/galeri/'.$data->galeri_gambar)}}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
				<img src="{{asset('public/pengunjung/images/galeri/'.$data->galeri_gambar)}}" class="img-fluid rounded">
			</a>
			@endforeach




		</div>
	</div>

</div>

@endsection


@section('js')
<script src="{{ asset('public/pengunjung/js/ekko-lightbox.js') }}"></script>
<script type="text/javascript">
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});
</script>
@endsection
