<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')

<style type="text/css">
.responsive {
  width: 100%;
  height: auto;
}
.pagination {
  display: inline-block;
}

.pagination li {
  color: white;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  border: 1px solid #ddd; /* Gray */

}

.pagination li:first-child {
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.pagination li:last-child {
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.pagination li.active {
  background-color: #964dbb;
  color: white;
  border: 1px solid #964dbb ;
}

.pagination li.active span {
  background-color: #ffffff;
  color: #ffffff;
}

.pagination li:hover:not(.active) {background-color: #ddd;}



</style>

@endsection


@section('content')

<div role="main" class="main">
  <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <h1>POTENSI <span></span></h1>
        </div>
        <div class="col-lg-6">
          <ul class="breadcrumb pull-right">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Potensi</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 pt-1 pb-4 mb-3">



        @foreach($potensi as $data)

        <article class="blog-post col">
          <div class="row">
            <div class="col-sm-12 col-lg-4">
              <div class="blog-post-image-wrapper">
                <a href="{{url('site_potensi')}}/{{$data->potensi_id}}"><img src="{{ asset('public/pengunjung/images/potensi/') }}/{{$data->potensi_gambar}} " alt="" class="img-fluid mb-4"></a>


                </span>
              </div>
            </div>
            <div class="col-sm-12 col-lg-8">
              <h4 class="font-weight-bold"><a href="{{url('site_potensi')}}/{{$data->potensi_id}}">{{$data->potensi_nama}}</a></h4>
              <hr class="solid mt-1 mb-1">
              <p style="overflow: hidden;display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                <?php $isi =  substr(strip_tags($data->potensi_isi),0,400); ?>
                {{$isi}} . . .
              </p>
            </div>
          </div>
        </article>

        @endforeach
        <br>
        <div class="" style="text-align: center;">
          {{$potensi->links()}}
        </div>




      </div>
    </div>
  </div>
</div>


@endsection


@section('js')
@endsection
