<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')

<style type="text/css">
.responsive {
  width: 100%;
  height: auto;
}

.pagination {
  display: inline-block;
}

.pagination li {
  color: white;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  border: 1px solid #ddd; /* Gray */

}

.pagination li:first-child {
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.pagination li:last-child {
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.pagination li.active {
  background-color: #964dbb;
  color: white;
  border: 1px solid #964dbb ;
}

.pagination li.active span {
  background-color: #ffffff;
  color: #ffffff;
}

.pagination li:hover:not(.active) {background-color: #ddd;}



</style>

@endsection


@section('content')

<div role="main" class="main">
  <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          @foreach($inovasi_layanan_detail as $nama)
          <h1>{{$nama->inovasi_layanan_nama}} <span>Inovasi Layanan</span></h1>
          @endforeach
        </div>
        <div class="col-lg-6">
          <ul class="breadcrumb pull-right">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="#">Inovasi Layanan</a></li>
            <li class="active">Detail</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 pt-1 pb-4 mb-3" style="text-align:justify;">

        @foreach($inovasi_layanan_detail as $detail)

        <p><img src="{{asset('public/pengunjung/images/inovasi_layanan')}}/{{$detail->inovasi_layanan_gambar}}" class="responsive" /></p>

        {!!$detail->inovasi_layanan_isi!!}

        @endforeach

      </div>
      <div class="col-lg-4 pt-1 pb-4 mb-3 center">
        <div class="heading heading-border heading-middle-border heading-middle-border-center">
          <h2><strong>Serupa</strong></h2>
        </div>
        <div class="owl-carousel owl-theme nav-bottom rounded-nav numbered-dots pl-1 pr-1" data-plugin-options="{'items': 1, 'loop': true, 'dots': true, 'nav': false}">

          @foreach($inovasi_layanan as $data)
          <div>
            <div class="custom-step-item">
              <span class="step text-uppercase">



              </span>
              <div class="step-content">
                <h4 class="mb-4"><a href="{{url('site_inovasi_layanan')}}/{{$data->inovasi_layanan_id}}"><strong>{{$data->inovasi_layanan_nama}}</strong></a></h4>

                <?php $isi =  substr($data->inovasi_layanan_isi, 0, 300); ?>
                {!!$isi!!} . . .

              </div>
            </div>
          </div>

          @endforeach

        </div>
      </div>
    </div>
  </div>
</div>



@endsection


@section('js')
@endsection
