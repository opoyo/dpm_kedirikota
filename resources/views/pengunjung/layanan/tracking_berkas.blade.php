<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>TRACKING BERKAS <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li><a href="#">Layanan</a></li>
						<li class="active">Tracking Berkas</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">
			<div class="clearfix"></div>
			<div class="container mb-4">
				<div class="row">
					<div class="col-md-12 center">
						<div class="heading heading-border heading-middle-border heading-middle-border-center">
							<h1>Masukan Nomer<strong> Resi</strong> / <strong>Pendaftaran</strong> Anda</h1>
						</div>
					</div>
				</div>
			</div>
			<form class="custom-contact-form-style-1">
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<i class="fas fa-list-alt icons text-color-primary"></i>
							<input type="text" value="" data-msg-required="Masukkan Nomer Resi/pendaftaran anda dengan benar." maxlength="100" class="form-control" name="name" id="name" placeholder="Nomer Resi/pendaftaran *" required="">
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col text-center">
						<input type="submit" value="Cari Sekarang" class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mb-4" data-loading-text="Loading...">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



@endsection


@section('js')
@endsection
