<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>PERIZINAN ONLINE <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li ><a href="#">Layanan</a></li>
						<li ><a href="#">Perizinan Online</a></li>
						<li class="active">Izin</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3 text-center">

			</div>
			<div class="col-lg-5 pt-1 pb-4 mb-3 text-center">
				<div class="feature-box-icon font-weight-semibold">
					<a target="_blank" href="https://kswi.kedirikota.go.id/frontend/">
						<img src="{{asset('public/pengunjung/images/login.png')}}" width="300" class="mx-2 my-2">
					</a>
				</div>
			</div>
			<div class="col-lg-2 pt-1 pb-4 mb-3 text-center">
				<div class="feature-box-icon font-weight-semibold"><img src="{{asset('public/pengunjung/images/atau.png')}}" width="100" class="custom-rounded-image mx-2 my-2"></div>
			</div>
			<div class="col-lg-5 pt-1 pb-4 mb-3 text-center">
				<div class="feature-box-icon font-weight-semibold">
					<a target="_blank" href="https://kswi.kedirikota.go.id/frontend/index.php//daftar/">
						<img src="{{asset('public/pengunjung/images/register.png')}}" width="300" class="mx-2 my-2">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection


@section('js')
@endsection
