<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
  <section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <h1>PANDUAN PELAKSANAAN OSS <span></span></h1>
        </div>
        <div class="col-lg-6">
          <ul class="breadcrumb pull-right">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="#">Layanan</a></li>
            <li><a href="#">Perizinan Online</a></li>
            <li class="active">OSS</li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <div class="container">
    <div class="row text-right mt-1" style="margin-bottom:30px;">
      <div class="col">
        <a style="margin-bottom:10px;margin-right:10px;" class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" target="_blank" href="{{asset('public/pengunjung/dokumen/Alur OSS.pdf')}}">Download Infografis</a>
        <a style="margin-bottom:10px;margin-right:10px;" class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase" target="_blank" href="https://www.oss.go.id/oss/">Buka www.oss.go.id</a>
      </div>
    </div>

    <div class="container mb-4">
      <div class="row">
        <div class="col-md-12 center">
          <div class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2><strong>INFOGRAFIS</strong> OSS</h2>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 pt-1 pb-4 mb-3">




          <div class="contact-form-wrap" style="margin-top: 20px;margin-bottom: 20px">
            <div align="center" data-animate="fadeInUp" data-delay=".7" style="animation-duration: 0.6s; animation-delay: 0.7s;" class="animated fadeInUp">
              <img align="center" src="{{asset('public/pengunjung/images/oss/1. alur OSS.png')}}" width="75%" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;" class="animated fadeInUp">
            </div>
          </div>
          
          <div class="contact-form-wrap" style="margin-top: 20px;margin-bottom: 20px">
            <div align="center" data-animate="fadeInUp" data-delay=".7" style="animation-duration: 0.6s; animation-delay: 0.7s;" class="animated fadeInUp">
              <img align="center" src="{{asset('public/pengunjung/images/oss/2. alur OSS.png')}}" width="75%" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;" class="animated fadeInUp">
            </div>
          </div>

          <div class="contact-form-wrap" style="margin-top: 20px;margin-bottom: 20px">
            <div align="center" data-animate="fadeInUp" data-delay=".7" style="animation-duration: 0.6s; animation-delay: 0.7s;" class="animated fadeInUp">
              <img align="center" src="{{asset('public/pengunjung/images/oss/3. alur OSS.png')}}" width="75%" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;" class="animated fadeInUp">
            </div>
          </div>

          <div class="contact-form-wrap" style="margin-top: 20px;margin-bottom: 20px">
            <div align="center" data-animate="fadeInUp" data-delay=".7" style="animation-duration: 0.6s; animation-delay: 0.7s;" class="animated fadeInUp">
              <img align="center" src="{{asset('public/pengunjung/images/oss/4. alur OSS.png')}}" width="75%" data-animate="fadeInUp" data-delay=".3" style="animation-duration: 0.6s; animation-delay: 0.3s;" class="animated fadeInUp">
            </div>
          </div>






        </div>
      </div>
    </div>
  </div>



  @endsection


  @section('js')
  @endsection
