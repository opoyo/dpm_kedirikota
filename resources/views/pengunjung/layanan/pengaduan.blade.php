<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>PENGADUAN <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li><a href="#">Layanan</a></li>
						<li class="active">Pengaduan</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">
			<div class="clearfix"></div>
				<div class="container mb-4">
					<div class="row">
						<div class="col-md-12 center">
							<div class="heading heading-border heading-middle-border heading-middle-border-center">
								<h1>FORMULIR <strong> PENGADUAN</strong></h1>
							</div>
						</div>
					</div>
					<p>Bapak/Ibu yang terhormat, Sampaikan keluhan anda tentang perizinan. Kami akan sangat senang apabila pengaduan tersebut disertai dengan bukti otentik. Jangan lupa lengkapi data anda agar kami dapat merespon dengan baik.</p>
				</div>
				<form class="custom-contact-form-style-1" action="{{url('site_pengaduan_post')}}" method="POST">
					{{ csrf_field() }}

          @if ($success == "1")
          <div class="alert alert-success"><ul>
            <b>Pengaduan Berhasil Terkirim !</b>
          </ul></div>
          @endif

				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_nama" id="pengaduan_nama" placeholder="Nama *" required />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_alamat" id="pengaduan_alamat" placeholder="Alamat *" required />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_kelurahan" id="pengaduan_kelurahan" placeholder="Kelurahan *" required />
						</div>
					</div>
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_kecamatan" id="pengaduan_kecamatan" placeholder="Kecamatan *" required />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_email" id="pengaduan_email" placeholder="Email *" required />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<input type="text" value="" maxlength="100" class="form-control dua" name="pengaduan_telp" id="pengaduan_telp" placeholder="Telepon *" required />
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<div class="custom-input-box">
							<textarea type="text"  class="form-control dua" name="pengaduan_data" id="pengaduan_data" placeholder="Isi Pengaduan" required></textarea>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col text-center">
						<input type="submit" value="Kirim Pengaduan" class="btn btn-outline custom-border-width btn-primary custom-border-radius font-weight-semibold text-uppercase mb-4" data-loading-text="Loading...">
					</div>
				</div>
				</form>
				<div class="container">
					<div class="row">
						<div class="col-md-12 center">
							<div class="heading heading-border heading-middle-border heading-middle-border-center">
								<h1>KOMUNIKASI <strong> MASYARAKAT</strong></h1>
							</div>
						</div>
					</div>
				</div>
				<div class="container mb-4">
					<div class="row">
						<div class="col-md-12 center">
							<ul class="comments">

								@foreach($pengaduan as $data)
								<li class="p-0">
									<div class="comment">
										<div class="comment-block">
											<div class="comment-arrow"></div>
											<span class="comment-by">
												<strong>{{$data->pengaduan_nama}}</strong>
												<span class="float-right">
													<span class="date float-right">{{ date('d M Y - H:i', $data->created_at->timestamp) }}</span>
												</span>
											</span>
											<p class="" style="font-size:14px;">{{$data->pengaduan_data}}</p>
										</div>
										<ul class="comments reply">
											<li>
												<div class="comment">
													<div class="img-thumbnail d-none d-sm-block p-0 no-borders">
														<img class="avatar hoverable" alt="" src="{{asset('public/pengunjung/images/staff/graphic_sabrina2.png')}}">
													</div>
													<div class="comment-block">
														<div class="comment-arrow"></div>
														<span class="comment-by">
															<strong>DPMPTSP</strong>
														</span>
														<p>{{$data->pengaduan_balas}}</p>
													</div>
												</div>
											</li>
										</ul><br />
									</div>
								</li>

								@endforeach

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection


@section('js')
@endsection
