<?php $hal = "home"; ?>
@extends('layouts.pengunjung.master')


@section('css')
@endsection


@section('content')

<div role="main" class="main">
	<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<h1>PERATURAN <span></span></h1>
				</div>
				<div class="col-lg-6">
					<ul class="breadcrumb pull-right">
						<li><a href="{{url('/')}}">Home</a></li>
						<li >Unduhan</li>
						<li class="active">Peraturan</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 pt-1 pb-4 mb-3">
				<div class="row mt-md mb-xl">

					<!-- @foreach($peraturan as $data)

					<div class="col-md-3">
						<div class="thumb-info custom-thumb-info-4">
							<div class="thumb-info-wrapper"><img src="{{asset('public/pengunjung/images/peraturan.png')}}" class="img-responsive"></div>
							<div class="thumb-info-caption custom-box-shadow" style="border-bottom: 1px solid #964dbb;">
								<div class="thumb-info-caption-text">
									<h5 class="text-center"><a target="_blank" href="{{asset('public/files/per')}}/{{$data->peraturan_file}}" class="text-color-dark">{{$data->peraturan_judul}}</a></h5>
								</div>
							</div>
						</div>
					</div>

					@endforeach -->

					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="width:10%">No #</th>
								<th style="width:70%">Nama Dokumen</th>
								<th style="width:10%">Download</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							@foreach($peraturan as $data)
							<tr>
								<td>{{$no}}.</td>
								<td>{{$data->peraturan_judul}}</td>
								<td><a target="_blank" href="{{asset('public/files/per')}}/{{$data->peraturan_file}}" class="text-color-dark"><i class="fa fa-download" aria-hidden="true"></i></a></td>
							</tr>
							<?php $no++; ?>
							@endforeach
						</tbody>
					</table>


			</div>
		</div>
	</div>
</div>
</div>

@endsection


@section('js')
@endsection
