<?php date_default_timezone_set('Asia/Jakarta'); ?>

<!DOCTYPE html>
<html lang="en" class="js touch history boxshadow csstransforms3d csstransitions video svg webkit chrome win js touch sticky-header-enabled">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta name="mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover,, shrink-to-fit=no" />

<link rel="apple-touch-icon" href="{{ asset('public/pengunjung/images/icon-64.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/pengunjung/images/icon-72-hdpi.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('public/pengunjung/images/icon-96-xhdpi.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('public/pengunjung/images/icon-96-xhdpi.png') }}">
<link rel="icon" type="image/x-icon" href="{{ asset('public/pengunjung/images/favicon.ico') }}" sizes="32x32">
<link rel="shortcut icon" href="{{ asset('public/pengunjung/images/favicon.ico') }}" type="image/x-icon" />
<!-- <link rel="canonical" href="{{ asset('public/pengunjung/bower_components/bootstrap/dist/css/bootstrap.min.css') }} //localhosthttp://[::1]/portal_kedirikota/index.php" />
<link rel='dns-prefetch' href='//localhost' /> -->
<!-- <link rel="alternate" type="application/rss+xml" title="Site" href="{{ asset('public/pengunjung/bower_components/bootstrap/dist/css/bootstrap.min.css') }} //localhosthttp://[::1]/portal_kedirikota/index.phpfeed/" /> -->

<title>DPM Kediri Kota</title>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CSintony:400,700" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
<link href="{{ asset('public/pengunjung/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" />
<link href="{{ asset('public/pengunjung/css/fontawesome-all.min.css') }}" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/animate.min.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/simple-line-icons.min.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/owl.theme.default.min.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/magnific-popup.min.css') }}" />
<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/theme.css') }}" />
<!-- Current Page CSS -->
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/settings.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/css/layers.css') }}" />
<link rel="stylesheet" href="{{ asset('public/pengunjung/bower_components/bootstrap/dist/css/navigation.css') }}" />
<!-- Head Libs -->
<script src="{{ asset('public/pengunjung/js/style.switcher.localstorage.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/modernizr.min.js') }}"></script>
<link href="{{ asset('public/pengunjung/css/site_custom.css') }}" type="text/css" rel="stylesheet" />

@yield('css')
<style>

body {
  font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; !important;
}

h1 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 32px; font-style: normal; font-variant: normal; font-weight: normal;; line-height: 26.4px; }
h2 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 24px; font-style: normal; font-variant: normal; font-weight: normal;; line-height: 26.4px; }
h3 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 18.72px; font-style: normal; font-variant: normal; font-weight: bold; line-height: 15.4px; }
h4 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: bold; line-height: 25px; }
h5 { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 13.28px; font-style: normal; font-variant: normal; font-weight: bold; line-height: 15.4px; }
blockquote { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 21px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 30px; }
pre { font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px; }

.footer-text{
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal !important; line-height: 15.4px;
}

.footer-sub li a{
	 font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal !important;
}

	.input-group {
		margin:0;
		padding:20px ;
		&:first-child { border-color: transparent; }
	}
	.form-control {
	padding: 0px 10px 0 20px;
	margin-top: 10px;
	color: #333;
	font-size: 28px;
	font-weight: 500;
		/* border: 1px solid #555; */
		-webkit-box-shadow: none;
		box-shadow: none;
		min-height:30px;
		height: auto;
		border-radius: 50px 0  0 50px !important;
	}
	.form-control :focus {
				-webkit-box-shadow: none;
				box-shadow: none;
				border-color: transparent;
		}
	#searchbtn
	{ border:0;
	padding: 0px 20px;
	margin-top: 10px;
	color: #fff;
	/* background:#888; */
	font-size: 20px;
	font-weight: 500;
		/* border: 1px solid #555; */
		border-left: none;
		-webkit-box-shadow: none;
		box-shadow: none;
		min-height:30px;
		height: auto;
	border-radius: 0 50px 50px 0 !important;
	}

	@media only screen and (max-width: 768px) {
  	/* For mobile phones: */
  	[class*="hilang"] {
    display: none;
  	}
	}
</style>
</head>
<body id="site" class="">
<div class="body">
<!-- <header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive custom-header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}"> -->
<header id="header" class="header-narrow custom-header-transparent-bottom-border" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
	<div class="header-body" style="background: #fff;">

		<div class="header-container container" style="max-width: 1230px;">
			<div class="header-row">

				<div class="header-column">
					<div class="header-row">
						<div class="col-md-5" style="padding-left: 0px; padding-right: 0px;">
							<div class="header-logo" style="color: #fff;">
								<a href="{{url('/')}}" style="font-size: 11px;">
									<img alt="" src="{{ asset('public/pengunjung/images/header_image.png') }}"  height="80px;"/>
									<!-- <b>DINAS PENANAMAN MODAL & PELAYANAN TERPADU SATU PINTU KOTA KEDIRI</b> -->
								</a>
							</div>
						</div>

						<div class="col-md-7 hilang" style="margin-left: -38px; padding-left: 0px; color: black;">
							<b style="font-size: 16px;">DINAS PENANAMAN MODAL & PELAYANAN TERPADU SATU PINTU </b>
							<br>
							<b style="font-size: 14px;"> PEMERINTAH KOTA KEDIRI</b>
						</div>

						<!-- <div class="header-logo" style="color: #fff;">
							<a href="http://[::1]/portal_kedirikota/site" style="font-size: 11px;">
								<img alt="" src="http://[::1]/portal_kedirikota/assets/images/pemkot_kediri.png"  height="50px;"/>
								<b>DINAS PENANAMAN MODAL & PELAYANAN TERPADU SATU PINTU KOTA KEDIRI</b>
							</a>
						</div> -->
					</div>
				</div>

				<div class="header-column justify-content-end">
					<div class="header-row">
						<form action="{{url('site_berita_cari')}}" method="post" role="search">
							{{csrf_field()}}
					    <div class="input-group">
						    <input class="form-control" placeholder="Search" name="cari" id="ed-srch-term" type="text" required>
					    	<div class="input-group-btn">
							    <button type="submit" id="searchbtn"><i class="fa fa-search" aria-hidden="true"></i></button>
					    	</div>
					    </div>
				    </form>
					</div>
				</div>
			</div>
		</div>


		<hr style="height: 4px; background: purple; margin-top: 10px; margin-bottom: 0px;">
		<div class="header-container container" style="background: #eee; max-width: 3000px; height: 40px;">
			<div class="header-column justify-content-end">
				<div class="header-row">
					<div class="header-nav header-nav-dark-dropdown">
						<div class="header-nav-main header-nav-main-square custom-header-nav-main-effect-1">
							<nav class="collapse">
								<ul class="nav flex-column flex-lg-row" id="mainNav">
									<li><a class="nav-link" href="{{url('/')}}">HOME</a></li>
									<li class="dropdown dropdown-primary"><a class="dropdown-toggle nav-link" href="#">TENTANG</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="{{url('site_selayang_pandang')}}">SELAYANG PANDANG</a></li>
											<li><a class="dropdown-item" href="{{url('site_tupoksi')}}">TUGAS POKOK & FUNGSI</a></li>
											<li><a class="dropdown-item" href="{{url('site_struktur_organisasi')}}">STRUKTUR ORGANISASI</a></li>
											<li><a class="dropdown-item" href="{{url('site_penghargaan')}}">PENGHARGAAN</a></li>
										</ul>
									</li>
									<li class="dropdown dropdown-primary"><a class="dropdown-toggle nav-link" href="#">LAYANAN</a>
										<ul class="dropdown-menu">
											<li class="dropdown-submenu">
												<a class="dropdown-item dropdown-toggle" href="#">PERIZINAN ONLINE</a>
												<ul class="dropdown-menu">
                          <!-- <li><a class="dropdown-item" target="_blank" href="https://www.oss.go.id/oss/">OSS</a></li> -->
                          <!-- <li><a class="dropdown-item" href="{{url('site_oss')}}">OSS</a></li> -->
                          <li><a class="dropdown-item" target="_blank" href="https://www.oss.go.id/oss/">OSS</a></li>
													<li><a class="dropdown-item" target="_blank" href="https://kswi.kedirikota.go.id/">KSWI</a></li>
													<!-- <li><a class="dropdown-item" href="{{url('site_komitmen')}}">PEMENUHAN KOMITMEN</a></li>
													<li><a class="dropdown-item" href="{{url('site_perizinan')}}">IZIN</a></li> -->
												</ul>
											</li>
											<!-- <li><a class="dropdown-item" href="{{url('site_tracking_berkas')}}">TRACKING BERKAS</a></li>
											<li><a class="dropdown-item" href="{{url('site_pencarian_kbli')}}">PENCARIAN KBLI</a></li>
                      <li><a class="dropdown-item" href="{{url('site_pengaduan')}}">PENGADUAN</a></li> -->
											<li><a class="dropdown-item" href="{{url('site_inovasi_layanan')}}">INOVASI LAYANAN</a></li>
										</ul>
									</li>
									<!-- <li><a class="nav-link" href="{{url('site_regulasi')}}">
										REGULASI
									</a></li> -->
                  <li><a class="nav-link" href="{{url('site_galeri')}}">
										GALERI
									</a></li>
									<li><a class="nav-link" href="{{url('site_potensi')}}">
										POTENSI
									</a></li>
									<li class="dropdown dropdown-primary"><a class="dropdown-toggle nav-link" href="#">UNDUHAN</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="{{url('site_panduan')}}">PANDUAN</a></li>
											<li><a class="dropdown-item" href="{{url('site_peraturan')}}">PERATURAN</a></li>
										</ul>
									</li>
                  <li><a class="nav-link" href="{{url('site_berita')}}">
										PUBLIKASI
									</a></li>
									<!-- <li class="dropdown dropdown-primary"><a class="dropdown-toggle nav-link" href="#">PUBLIKASI</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="{{url('site_berita')}}">BERITA</a></li>
											<li><a class="dropdown-item" href="{{url('site_laporan')}}">LAPORAN</a></li>
										</ul>
									</li> -->
								</ul>
							</nav>
						</div>
						<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
							<i class="fas fa-bars"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="header-row">
				<!-- <div class="header-column">
					<div class="header-row">
						<div class="header-logo" style="color: #fff;">
							<a href="http://[::1]/portal_kedirikota/site" style="font-size: 11px;">
								<img alt="" src="http://[::1]/portal_kedirikota/assets/images/pemkot_kediri.png"  height="50px;"/>
								<b>DINAS PENANAMAN MODAL & PELAYANAN TERPADU SATU PINTU KOTA KEDIRI</b>
							</a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</header>

@yield('content')


<!-- <section class="section section-text-light section-background m-0" style="background: url('http://[::1]/portal_kedirikota/assets/images/footer-construction.jpg'); background-size: cover;"> -->
<section class="section section-text-light section-background m-0" style="background: #755da9; background-size: cover;">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 custom-sm-margin-top">
				<div class="row">
					<div class="col-md-3">
						<h5 class="footer-text">TENTANG</h5>
						<ul class="list list-icons list-icons-sm font-weight-bold footer-sub">
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_selayang_pandang')}}" class="white-text">Selayang Pandang</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_tupoksi')}}" class="white-text">Tugas Pokok & Fungsi</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_struktur_organisasi')}}" class="white-text">Struktur Organisasi</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_penghargaan')}}" class="white-text">Penghargaan</a></li>
						</ul>
						<br>
						<img src="{{asset('public/pengunjung/images/dpm1.png')}}" class="img-responsive" alt="" style="width: 270px; height: 110px;">
					</div>
					<div class="col-md-3">
						<h5 class="footer-text">LAYANAN</h5>
						<ul class="list list-icons list-icons-sm font-weight-bold footer-sub"><!-- <li><i class="fa fa-caret-right"></i> <a href="https://mail.pertamina.com/owa/">Webmail</a></li> -->
              <li><i class="fa fa-caret-right"></i> <a target="_blank" href="https://www.oss.go.id/oss/" class="white-text">OSS</a></li>
              <li><i class="fa fa-caret-right"></i> <a target="_blank" href="https://kswi.kedirikota.go.id/" class="white-text">KSWI</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_inovasi_layanan')}}" class="white-text">Inovasi Layanan</a></li>
							<!-- <li><i class="fa fa-caret-right"></i> <a href="{{url('site_tracking_berkas')}}" class="white-text">Tracking Berkas</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_pencarian_kbli')}}" class="white-text">Pencarian KBLI</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_pengaduan')}}" class="white-text">Pengaduan</a></li> -->
						</ul>
					</div>
					<div class="col-md-3">
						<a href="{{url('site_potensi')}}"><h5 class="footer-text">POTENSI</h5></a>
						<h5 class="footer-text">UNDUHAN</h5>
						<ul class="list list-icons list-icons-sm font-weight-bold footer-sub">
							<li><i class="fa fa-caret-right"></i> <a href=" {{url('site_panduan')}}" class="white-text">Panduan</a></li>
							<li><i class="fa fa-caret-right"></i> <a href=" {{url('site_peraturan')}}" class="white-text">Peraturan</a></li>
						</ul>
					</div>
					<div class="col-md-3">
            <a href="{{url('site_galeri')}}"><h5 class="footer-text">GALERI</h5></a>
            <a href="{{url('site_berita')}}"><h5 class="footer-text">PUBLIKASI</h5></a>
						<ul class="list list-icons list-icons-sm font-weight-bold footer-sub">
							<!-- <li><i class="fa fa-caret-right"></i> <a href="{{url('site_berita')}}" class="white-text">Berita</a></li>
							<li><i class="fa fa-caret-right"></i> <a href="{{url('site_laporan')}}" class="white-text">Laporan</a></li> -->
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<h5 class="font-weight-bold footer-text">Hubungi Kami</h5>
				<p class="custom-opacity-font"></p>
				<div class="row">
					<div class="col-lg-12">
						<h4 class="mb-1 text-4">Alamat</h4>
						<span class="custom-call-to-action-2 text-color-light text-2 custom-opacity-font">
							<span class="info text-3">
								Jl. Basuki Rahmad No.15 <br />Kota Kediri
							</span>
						</span>
					</div>
					<div class="col-lg-12">
						<h4 class="mb-1 text-4">Telepon</h4>
						<a href="tel:+62354682345" class="text-decoration-none" target="_blank" title="Call Us">
							<span class="custom-call-to-action-2 text-color-light text-2 custom-opacity-font">
								<span class="info text-3">
									(0354) 682345
								</span>
							</span>
						</a>
					</div>
					<div class="col-lg-12">
						<h4 class="mb-1 text-4">Email</h4>
						<a href="mail:info@dpmptsp.kedirikota.go.id" class="text-decoration-none" target="_blank" title="Mail Us">
							<span class="custom-call-to-action-2 text-color-light text-2 custom-opacity-font">
								<span class="info text-3">
									info@dpmptsp.kedirikota.go.id
								</span>
							</span>
						</a>
					</div>
					<div class="col-lg-12">
						<h4 class="mb-1 text-4">Media Sosial</h4>
						<ul class="social-icons custom-social-icons-style-1 mt-2 custom-opacity-font">
							<li class="social-icons-instagram">
								<a href="https://www.instagram.com/dpmptspkotakediri/?hl=id" target="_blank" title="Instagram">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
							<li class="social-icons-facebook">
								<a href="https://www.facebook.com/dpmptspkedirikota" target="_blank" title="Facebook">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li class="social-icons-youtube">
								<a href="https://www.youtube.com/channel/UCclzH8UOi24s0duCywBcuGw" target="_blank" title="Youtube">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
							<li class="social-icons-twitter">
								<a href="https://twitter.com/bpmkotakediri" target="_blank" title="Twitter">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<footer id="footer" style="background: #eee;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- <p style="color: #000;">2018 © EGREF <span>IT Consulting</span> - Copyright All Rights Reserved</p> -->
				<p style="color: #000;"><i>Copyright ©</i> 2018 - <span><b>DPM DAN PTSP KOTA KEDIRI</b></span> </p>
			</div>
		</div>
	</div>
</footer>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{ asset('public/pengunjung/js/jquery.appear.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery-cookie.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/popper.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/common.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.validation.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.easy-pie-chart.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.gmap.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.lazyload.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/vide.min.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/theme.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/pengunjung/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/pengunjung/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/pengunjung/js/js_custom.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/view.contact.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/demo-business-consulting.js') }}"></script>
<script src="{{ asset('public/pengunjung/js/theme.init.js') }}"></script>
@yield('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('#global-modal').modal('show');
	});
</script>
</body>
</html>
