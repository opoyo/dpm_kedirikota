<?php date_default_timezone_set('Asia/Jakarta'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('public/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('public/admin/bower_components/font-awesome/css/font-awesome.min.css') }} ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('public/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('public/admin/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('public/admin/dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{asset('public/admin/dist/css/sweetalert.css')}}">


  <link rel="icon" type="image/x-icon" href="{{ asset('public/pengunjung/images/favicon.ico') }}" sizes="32x32">
  <link rel="shortcut icon" href="{{ asset('public/pengunjung/images/favicon.ico') }}" type="image/x-icon" />

<style media="screen">
.btn-primary:hover {
background-color: #960396;
color: white;
}
.btn-primary:focus{
  background-color: #960396;
  color: white;
}
</style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @yield('css')

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <span class="logo" style="background-color:#960396">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>PM</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>DPM</b> Kediri</span>
      </span>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top"  style="background-color:#a908a9;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{asset('public/pengunjung/images/person-1824144_960_720.png')}}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{Auth::user()->name}}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{asset('public/pengunjung/images/person-1824144_960_720.png')}}" class="img-circle" alt="User Image">

                  <p>
                    {{Auth::user()->email}}
                    <!-- <small>Member since Nov. 2012</small> -->
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="{{route('admin_user.index')}}" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="icon ion-power"></i>
                    Sign Out
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <img style="width:190px; height:80px;" src="{{ asset('public/pengunjung/images/dpm1.png') }}" class="img-square" alt="User Image">

        <!-- <div class="pull-left image">
          <img src="{{ asset('public/pengunjung/images/pemkot_kediri.png') }}" class="img-square" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div> -->
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <!-- Menu ====================================================================================================================== -->

        <li class="<?php if($hal == "index") echo "active" ; ?>">
          <a href="{{url('index_admin.html')}}">
            <i class="fa fa-home"></i> <span>Beranda</span>
          </a>
        </li>

        <li class="treeview <?php if(($hal == "admin_slideshow")||($hal == "admin_popup")) echo "active" ; ?>">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Gambar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($hal == "admin_slideshow") echo "active" ; ?>"><a href="{{url('admin_slideshow')}}"><i class="fa fa-circle-o"></i>Slideshow</a></li>
            <li class="<?php if($hal == "admin_popup") echo "active" ; ?>"><a href="{{url('admin_popup')}}"><i class="fa fa-circle-o"></i>Popup</a></li>

          </ul>
        </li>

        <li class="treeview <?php if(($hal == "admin_selayang_pandang")||($hal == "admin_tupoksi")||($hal == "admin_struktur_organisasi")||($hal == "admin_penghargaan")) echo "active" ; ?>">
          <a href="#">
            <i class="fa  fa-address-card-o"></i> <span>Tentang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($hal == "admin_selayang_pandang") echo "active" ; ?>"><a href="{{url('admin_selayang_pandang')}}"><i class="fa fa-circle-o"></i>Selayang Pandang</a></li>
            <li class="<?php if($hal == "admin_tupoksi") echo "active" ; ?>"><a href="{{url('admin_tupoksi')}}"><i class="fa fa-circle-o"></i>Tugas Pokok & Fungsi</a></li>
            <li class="<?php if($hal == "admin_struktur_organisasi") echo "active" ; ?>"><a href="{{url('admin_struktur_organisasi')}}"><i class="fa fa-circle-o"></i>Struktur Organisasi</a></li>
            <li class="<?php if($hal == "admin_penghargaan") echo "active" ; ?>"><a href="{{url('admin_penghargaan')}}"><i class="fa fa-circle-o"></i>Penghargaan</a></li>

          </ul>
        </li>

      <!-- static -->
      <li class="<?php if($hal == "admin_berita") echo "active" ; ?>">
        <a href="{{url('admin_berita')}}">
          <i class="fa fa-newspaper-o"></i> <span>Berita</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_video") echo "active" ; ?>">
        <a href="{{url('admin_video')}}">
          <i class="fa fa-youtube-play"></i> <span>Video</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_potensi") echo "active" ; ?>">
        <a href="{{url('admin_potensi')}}">
          <i class="fa fa-star-half-full "></i> <span>Potensi</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_inovasi_layanan") echo "active" ; ?>">
        <a href="{{url('admin_inovasi_layanan')}}">
          <i class="fa fa-bus "></i> <span>Inovasi Layanan</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_galeri") echo "active" ; ?>">
        <a href="{{url('admin_galeri')}}">
          <i class="fa fa-image "></i> <span>Galeri</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_panduan") echo "active" ; ?>">
        <a href="{{url('admin_panduan')}}">
          <i class="fa fa-book"></i> <span>Panduan</span>
        </a>
      </li>
      <li class="<?php if($hal == "admin_peraturan") echo "active" ; ?>">
        <a href="{{url('admin_peraturan')}}">
          <i class="fa fa-file-text-o"></i> <span>Peraturan</span>
        </a>
      </li>
      <!-- <li class="<?php if($hal == "admin_penghargaan") echo "active" ; ?>">
        <a href="{{url('admin_penghargaan')}}">
          <i class="fa fa-address-card-o"></i> <span>Penghargaan</span>
        </a>
      </li> -->
      <!-- <li class="<?php if($hal == "admin_pengaduan") echo "active" ; ?>">
        <a href="{{url('admin_pengaduan')}}">
          <i class="fa fa-envelope"></i> <span>Pengaduan</span>
        </a>
      </li> -->
      <li class="<?php if($hal == "admin_biodata_pejabat") echo "active" ; ?>">
        <a href="{{url('master_biodata_pejabat')}}">
          <i class="fa fa-address-book"></i> <span>Master Biodata Pejabat</span>
        </a>
      </li>



      <li class="<?php if($hal == "master_user") echo "active" ; ?>">
        <a href="{{url('master_user')}}">
          <i class="fa fa-user-circle-o"></i> <span>Master User</span>
        </a>
      </li>

      <li class="<?php if($hal == "admin_panduan_aplikasi") echo "active" ; ?>">
        <a href="{{url('admin_panduan_aplikasi')}}">
          <i class="fa  fa-black-tie"></i> <span>Panduan Aplikasi</span>
        </a>
      </li>


      <!-- End Menu ====================================================================================================================== -->


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


  @yield('content')

</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    {{-- <b>Version</b> 1 --}}
  </div>
  <strong>Copyright &copy; 2019 <a href="#">DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU KOTA KEDIRI</a>.</strong>
</footer>
{{-- <footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.4.0
  </div>
  <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
  reserved.
</footer> --}}

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('public/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('public/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/admin/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('public/admin/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/validator.js') }}"></script>


@yield('js')

<script>
$(document).ready(function () {
  $('.sidebar-menu').tree()
})
</script>
</body>
</html>
