<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

Route::resource('/', 'PengunjungHomeController');

//Selayang Pandang====================================================
Route::get('site_selayang_pandang', 'PengunjungSelayangPandangController@index');

//Tugas pokok dan fungsi====================================================
Route::get('site_tupoksi', 'PengunjungTupoksiController@index');

//Struktur Organisasi====================================================
Route::get('site_struktur_organisasi', 'PengunjungStrukturOrganisasiController@index');

//Penghargaan====================================================
Route::get('site_penghargaan', 'PengunjungPenghargaanController@index');

//OSS====================================================
Route::get('site_oss', 'PengunjungOssController@index');

//Pemenuhan Komitmen====================================================
Route::get('site_komitmen', 'PengunjungKomitmenController@index');

//Perizinan Online====================================================
Route::get('site_perizinan', 'PengunjungPerizinanController@index');

//Tracking Berkas====================================================
Route::get('site_tracking_berkas', 'PengunjungTrackingBerkasController@index');

//Pencarian KBLI====================================================
Route::get('site_pencarian_kbli', 'PengunjungPencarianKbliController@index');

//Pengaduan====================================================
Route::get('site_pengaduan', 'PengunjungPengaduanController@index');
Route::post('site_pengaduan_post', 'PengunjungPengaduanController@store');

//Regulasi====================================================
Route::get('site_regulasi', 'PengunjungRegulasiController@index');

//Potensi====================================================
Route::get('site_potensi', 'PengunjungPotensiController@index');
Route::get('site_potensi/{id}', 'PengunjungPotensiController@detail');

//Panduan====================================================
Route::get('site_panduan', 'PengunjungPanduanController@index');

//Peraturan====================================================
Route::get('site_peraturan', 'PengunjungPeratuanController@index');

//BERITA====================================================
Route::get('site_berita', 'PengunjungBeritaController@index');
Route::get('site_berita/{id}', 'PengunjungBeritaController@detail');
Route::post('site_berita_cari', 'PengunjungBeritaController@cari');

//Peraturan====================================================
Route::get('site_laporan', 'PengunjungLaporanController@index');

//Galeri====================================================
Route::get('site_galeri', 'PengunjungGaleriController@index');
Route::get('site_galeri/{id}', 'PengunjungGaleriController@detail');

//Inovasi Layanan====================================================
Route::get('site_inovasi_layanan', 'PengunjungInovasiLayananController@index');
Route::get('site_inovasi_layanan/{id}', 'PengunjungInovasiLayananController@detail');

//  ================================================ ADMIN ==========================================================

Route::group(['middleware' => 'auth'], function () {

  Route::get('index_admin.html', 'PecahTemplateAdminController@index');
  Route::resource('admin_user', 'AdminUserController');

  //  ================================================ MASTER USER ==========================================================
    Route::get('data_master_user', 'MasterUserController@listData')->name('data_master_user');
    Route::resource('master_user', 'MasterUserController');
  //  ================================================ END MASTER USER ==========================================================
  //  ================================================ BERITA ==========================================================
    Route::resource('admin_berita', 'AdminBeritaController');
  //  ================================================ END BERITA ==========================================================
  //  ================================================ PANDUAN ==========================================================
    Route::resource('admin_panduan', 'AdminPanduanController');
  //  ================================================ END PANDUAN ==========================================================
  //  ================================================ PERATURAN ==========================================================
    Route::resource('admin_peraturan', 'AdminPeraturanController');
  //  ================================================ END PERATURAN ==========================================================
  //  ================================================ PENGHARGAAN ==========================================================
    Route::resource('admin_penghargaan', 'AdminPenghargaanController');
  //  ================================================ END PENGHARGAAN ==========================================================
  //  ================================================ SLIDESHOW ==========================================================
    Route::resource('admin_slideshow', 'AdminSlideshowController');
  //  ================================================ END SLIDESHOW ==========================================================
  //  ================================================ Popup ==========================================================
    Route::resource('admin_popup', 'AdminPopupController');
    Route::post('admin_popup/update_aktif', 'AdminPopupController@update_checkbox');
  //  ================================================ END Popup ==========================================================
    // Route::get('data_master_biodata_pejabat', 'AdminBiodataPejabatController@listData')->name('data_master_biodata_pejabat');
    Route::resource('master_biodata_pejabat', 'AdminBiodataPejabatController');
  //  ================================================ END Popup ==========================================================
    Route::post('admin_pengaduan/update_aktif', 'AdminPengaduanController@update_checkbox');
    Route::post('admin_pengaduan/balas', 'AdminPengaduanController@balas_pengaduan');
    Route::resource('admin_pengaduan', 'AdminPengaduanController');


  //================================================= Admin Video =========================================================
  Route::get('data_admin_video', 'AdminVideoController@listData')->name('data_admin_video');
  Route::resource('admin_video', 'AdminVideoController');
  //================================================= Admin Video =========================================================

  //  ================================================ POTENSI ==========================================================
    Route::resource('admin_potensi', 'AdminPotensiController');
  //  ================================================ END POTENSI ==========================================================

  //  ================================================ INOVASI LAYANAN ==========================================================
    Route::resource('admin_inovasi_layanan', 'AdminInovasiLayananController');
  //  ================================================ END INOVASI LAYANAN ==========================================================

  //  ================================================ Galeri ==========================================================
    Route::resource('admin_galeri', 'AdminGaleriController');
  //  ================================================ END Galeri ==========================================================

  //  ================================================ END ADMIN ==========================================================

  //  ================================================ SLIDESHOW ==========================================================
    Route::get('admin_panduan_aplikasi', 'AdminPanduanAplikasiController@index');
  //  ================================================ END SLIDESHOW ==========================================================


  //  ================================================ Selayang pandang ==========================================================
    Route::resource('admin_selayang_pandang', 'AdminSelayangPandangController');
  //  ================================================ END Selayang pandang ==========================================================

  //  ================================================ Tupoksi ==========================================================
    Route::resource('admin_tupoksi', 'AdminTupoksiController');
  //  ================================================ END Tupoksi ==========================================================

  //  ================================================ Struktur Organisasi ==========================================================
    Route::resource('admin_struktur_organisasi', 'AdminStrukturOrganisasiController');
  //  ================================================ END Struktur Organisasi ==========================================================



});
